<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Travelport Apps | Travdoc </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Travelport Kenya Travdoc Itinerary Viewer" name="description" />
        <meta content="Dew CIS Solutions" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="./assets/global/plugins/font-oswald/oswald.css" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="./assets/global/plugins/font-opensans/opensans.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="./assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="./assets/pages/css/doc-2.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="./assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="./assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="./assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/layouts/layout5/css/custom.css" rel="stylesheet" type="text/css" />
		<link href="./assets/layouts/layout5/css/print.css" rel="stylesheet" type="text/css" media="print" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        <div class="clearfix navbar-fixed-top">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
                            <!-- BEGIN LOGO -->
                            <a id="index" class="page-logo" href="index.html">
                                <img src="./assets/layouts/layout5/img/logo.png" alt="Logo"> </a>
                            <!-- END LOGO -->
                            <!-- BEGIN TOPBAR ACTIONS -->
                            <div class="topbar-actions">


                                <!-- BEGIN USER PROFILE -->
                                <div class="btn-group-img btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span>Hi, Marcus</span>
                                        <img src="./assets/layouts/layout5/img/avatar1.jpg" alt=""> </button>
                                </div>
                                <!-- END USER PROFILE -->
                                <!-- BEGIN GROUP ACTIONS -->
                                <div class="btn-group-red btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="fa fa-gear"></i>
                                    </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li class="active">
                                            <a href="#">New Post</a>
                                        </li>
                                        <li>
                                            <a href="#">New Comment</a>
                                        </li>
                                        <li>
                                            <a href="#">Share</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="#">Comments
                                                <span class="badge badge-success">4</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">Feedbacks
                                                <span class="badge badge-danger">2</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- END GROUP INFORMATION -->
                            </div>
                            <!-- END TOPBAR ACTIONS -->
                        </div>
                    </div>
                    <!--/container-->
                </nav>
            </header>
            <!-- END HEADER -->
            <div class="container">
                <div class="page-content">

                    <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
                    <div class="page-content-container">
                        <div class="page-content-row">
                            <!-- BEGIN PAGE SIDEBAR -->

                            <!-- END PAGE SIDEBAR -->
                            <div class="page-content-col">
                                <!-- BEGIN PAGE BASE CONTENT -->
                                <div class="doc-content-2 bordered">
                                    <div class="row doc-head">
                                        <div class="col-md-4 col-xs-6">
                                            <div class="doc-logo">
                                                <img src="./assets/pages/img/logos/logo.png" class="img-responsive" alt="" />
                                             </div>
                                        </div>
                                        <div class="col-md-4 col-xs-3">
                                            <div class="company-address">
                                                <span class="bold uppercase">Dew CIS Travel Solutions Ltd.</span>
                                                <br/> Barclays Plaza, 12th Floor
                                                <br/> Loita Street, 
												<br/> P.O Box 45689 - 00100 Nairobi
                                                <br/>
                                                <span class="bold">T</span> +254 20 222 7100
                                                <br/>
                                                <span class="bold">E</span> reservations@dewcis.com
                                                <br/>
                                                <span class="bold">W</span> www.dewcis.com 
											</div>
                                        </div>
                                        <div class="col-md-4 col-xs-3">
                                            <div class="company-address">
                                                <span class="bold uppercase">Travel Consultant Information</span>
                                                <br/><span class="bold">Name </span>Emily Bundi Kuene
                                                <br/>
                                                <span class="bold">Mobile </span> +254 734 254289
                                                <br/>
                                                <span class="bold">Email </span> reservations@dewcis.com
												<br/>
											</div>
                                        </div>
                                    </div>
									<div class="row text-center">
										<h1>My Trip</h1>
									</div>
                                    <div class="row doc-cust-add">
                                        <div class="col-xs-3">
                                            <h2 class="doc-title uppercase">Traveller(s) Name</h2>
                                            <p class="doc-desc">OTIENO FRANCISMR</p>
                                        </div>
                                        <div class="col-xs-3">
                                            <h2 class="doc-title uppercase">Issue Date</h2>
                                            <p class="doc-desc">Nov 12, 2015</p>
                                        </div>
                                        <div class="col-xs-3">
                                            <h2 class="doc-title uppercase">Booking Reference</h2>
                                            <p class="doc-desc">AZX67M</p>
                                        </div>
                                        <div class="col-xs-3">
                                            <h2 class="doc-title uppercase">Reward Program</h2>
                                            <p class="doc-desc">45678912345</p>
                                        </div>
                                    </div>
                                    <div class="row doc-body">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-plane"></i>   Flight 1  - Kenya Airways - Nairobi to Entebbe - Confirmed 
												</div>
                                            </div>
                                            <div class="portlet-body">
												<div class="row">
													<div class="col-md-12 ">
														<div class="table-scrollable table-scrollable-borderless">
															<table class="table table-hover table-light">
													                <thead>
													                    <tr class="uppercase">
													                        <th>  Flight Number </th>
													                        <th>  Aircraft </th>
																			<th>  Cabin  </th>
																			<th>  Class  </th>
													                        <th>  Confirmation Code </th>
													                    </tr>
													                </thead>
													                <tbody>
													                    <tr>
													                        <td> KQ600  </td>
													                        <td> Boeing 747 </td>
																			<td> Economy </td>
																			<td> M </td>
													                        <td> KI12345  </td>
													                    </tr>
													                </tbody>
															</table>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-5">
														<div class="portlet light">
														    <div class="portlet-title">
														        <div class="caption">
														            <i class="icon-puzzle font-grey-gallery"></i>
														            <span class="caption-subject bold font-grey-gallery uppercase"> Departure </span>
														            <span class="caption-helper"> Nairobi </span>
														        </div>
														    </div>
														    <div class="portlet-body">
														        <h4> Saturday 17 Sep 2016 16:00 </h4>
														        <p> Jomo Kenyatta International Airport, Nairobi Kenya <br> Terminal 1E </p>
														    </div>
														</div>														
													</div>
													<div class="col-xs-2">
														<div class="tiles">
															<div class="tile bg-blue-steel">
														    	<div class="tile-body">
														        	<i class="fa fa-plane"></i>
														    	</div>
														    	<div class="tile-object">
														        	<div class="name"> Non Stop </div>
														        	<div class="number"> 45 min </div>
														    	</div>
															</div>
														</div>
													</div>
													<div class="col-xs-5">
														<div class="portlet light">
														    <div class="portlet-title">
														        <div class="caption">
														            <i class="icon-puzzle font-grey-gallery"></i>
														            <span class="caption-subject bold font-grey-gallery uppercase"> Arrival </span>
														            <span class="caption-helper"> Entebbe </span>
														        </div>
														    </div>
														    <div class="portlet-body">
														        <h4> Saturday 17 Sep 2016 17:00 </h4>
														        <p> Entebbe Airport, Entebbe Uganda <br> Terminal 2 </p>
														    </div>
														</div>													
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="table-scrollable">
												            <table class="table table-condensed table-hover">
												                <thead>
												                    <tr>
												                        <th> Passenger </th>
												                        <th> E-Ticket No. </th>
												                        <th> Seat No. </th>
												                        <th> Baggage </th>
												                        <th> Meal </th>
																		<th> Carbon Emission </th>
												                    </tr>
												                </thead>
												                <tbody>
												                    <tr>
												                        <td> OTIENO FRANCISMR </td>
												                        <td> 1234567890 </td>
												                        <td> 2B </td>
												                        <td> *2P </td>
												                        <td> Vegetarian Weeds</td>
												                		<td> 300 Kg </td>
												                    </tr>
												                    <tr>
												                        <td> OTIENO IMMACULATEMRS </td>
												                        <td> 1234567891 </td>
												                        <td> 2B </td>
												                        <td> *2P </td>
												                        <td> Meat Only </td>
												              			<td> 400 Kg </td>
												                    </tr>

												                </tbody>
												            </table>
														</div>																											
													</div>
												</div>
												<div class="row">
													<div class="note note-info">
													    <h4 class="block">Special Services Requests</h4>
													    <p> Wheelchair is needed </p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-10">
														<p class="bold"> *Vendor Remarks can be added here  </p>
													</div>
												</div>
                                            </div>
										</div>
									</div>
                                    <div class="row doc-body">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-plane"></i>   Flight 1  - Kenya Airways - Nairobi to Entebbe - Confirmed 
												</div>
                                            </div>
                                            <div class="portlet-body">
												<div class="row">
													<div class="col-md-12 ">
														<div class="table-scrollable table-scrollable-borderless">
															<table class="table table-hover table-light">
													                <thead>
													                    <tr class="uppercase">
													                        <th>  Flight Number </th>
													                        <th>  Aircraft </th>
																			<th>  Cabin  </th>
																			<th>  Class  </th>
													                        <th>  Confirmation Code </th>
													                    </tr>
													                </thead>
													                <tbody>
													                    <tr>
													                        <td> KQ600  </td>
													                        <td> Boeing 747 </td>
																			<td> Economy </td>
																			<td> M </td>
													                        <td> KI12345  </td>
													                    </tr>
													                </tbody>
															</table>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-5">
														<div class="portlet light">
														    <div class="portlet-title">
														        <div class="caption">
														            <i class="icon-puzzle font-grey-gallery"></i>
														            <span class="caption-subject bold font-grey-gallery uppercase"> Departure </span>
														            <span class="caption-helper"> Nairobi </span>
														        </div>
														    </div>
														    <div class="portlet-body">
														        <h4> Saturday 17 Sep 2016 16:00 </h4>
														        <p> Jomo Kenyatta International Airport, Nairobi Kenya <br> Terminal 1E </p>
														    </div>
														</div>														
													</div>
													<div class="col-xs-2">
														<div class="tiles">
															<div class="tile bg-blue-steel">
														    	<div class="tile-body">
														        	<i class="fa fa-plane"></i>
														    	</div>
														    	<div class="tile-object">
														        	<div class="name"> Non Stop </div>
														        	<div class="number"> 45 min </div>
														    	</div>
															</div>
														</div>
													</div>
													<div class="col-xs-5">
														<div class="portlet light">
														    <div class="portlet-title">
														        <div class="caption">
														            <i class="icon-puzzle font-grey-gallery"></i>
														            <span class="caption-subject bold font-grey-gallery uppercase"> Arrival </span>
														            <span class="caption-helper"> Entebbe </span>
														        </div>
														    </div>
														    <div class="portlet-body">
														        <h4> Saturday 17 Sep 2016 17:00 </h4>
														        <p> Entebbe Airport, Entebbe Uganda <br> Terminal 2 </p>
														    </div>
														</div>													
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="table-scrollable">
												            <table class="table table-condensed table-hover">
												                <thead>
												                    <tr>
												                        <th> Passenger </th>
												                        <th> E-Ticket No. </th>
												                        <th> Seat No. </th>
												                        <th> Baggage </th>
												                        <th> Meal </th>
																		<th> Carbon Emission </th>
												                    </tr>
												                </thead>
												                <tbody>
												                    <tr>
												                        <td> OTIENO FRANCISMR </td>
												                        <td> 1234567890 </td>
												                        <td> 2B </td>
												                        <td> *2P </td>
												                        <td> Vegetarian Weeds</td>
												                		<td> 300 Kg </td>
												                    </tr>
												                    <tr>
												                        <td> OTIENO IMMACULATEMRS </td>
												                        <td> 1234567891 </td>
												                        <td> 2B </td>
												                        <td> *2P </td>
												                        <td> Meat Only </td>
												              			<td> 400 Kg </td>
												                    </tr>

												                </tbody>
												            </table>
														</div>																											
													</div>
												</div>
												<div class="row">
													<div class="note note-info">
													    <h4 class="block">Special Services Requests</h4>
													    <p> Wheelchair is needed </p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-10">
														<p class="bold"> *Vendor Remarks can be added here  </p>
													</div>
												</div>
                                            </div>
										</div>
									</div>
									<div class-"row doc-body">
										<div class="row doc-body">
										    <div class="portlet box blue">
										        <div class="portlet-title">
										            <div class="caption">
										                <i class="fa fa-plane"></i> Itinerary Remarks
													</div>
										        </div>
										        <div class="portlet-body">
													<div class="row">
														<div class="col-md-12 ">
														    <p> Wheelchair is needed </p>
														</div>
													</div>
										        </div>
											</div>
										</div>
									</div>
									<div class-"row doc-body">
										<div class="row doc-body">
										    <div class="portlet box blue">
										        <div class="portlet-title">
										            <div class="caption">
										                <i class="fa fa-plane"></i> Important Information
													</div>
										        </div>
										        <div class="portlet-body">
													<div class="row">
														<div class="col-md-12 ">
															<h4>Flight Notes</h4>
														    <p> 
																<ul>
																    <li> Lorem ipsum dolor sit amet </li>
																    <li> Consectetur adipiscing elit </li>
																    <li> Nulla volutpat aliquam velit
																        <ul>
																            <li> Phasellus iaculis neque </li>
																            <li> Purus sodales ultricies </li>
																            <li> Vestibulum laoreet porttitor sem </li>
																            <li> Ac tristique libero volutpat at </li>
																        </ul>
																    </li>
																</ul>
															 </p>
														</div>
													</div>
										        </div>
											</div>
										</div>
									</div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <a class="btn btn-lg green-haze hidden-print uppercase print-btn" onclick="javascript:window.print();">Print</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PAGE BASE CONTENT -->
                            </div>
                        </div>
                    </div>
                    <!-- END SIDEBAR CONTENT LAYOUT -->
                </div>
                <!-- BEGIN FOOTER -->
                <p class="copyright"> 2016 &copy; Travelport Kenya Travdoc. &nbsp;|&nbsp; Developed by
                    <a target="_blank" href="http://www.dewcis.com">Dew CIS Solutions</a> 
                </p>
                <a href="#index" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>
                <!-- END FOOTER -->
            </div>
        </div>
        <!-- END CONTAINER -->


        <!--[if lt IE 9]>
<script src="./assets/global/plugins/respond.min.js"></script>
<script src="./assets/global/plugins/excanvas.min.js"></script> 
<script src="./assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="./assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="./assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="./assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="./assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="./assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
        <script src="./assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="./assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>