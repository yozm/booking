{
   "PNRBFRetrieve": {
      "ErrorCode": "0003",
      "Control": {
         "KLRCnt": "10",
         "KlrAry": [
            {
               "ID": "BP10",
               "NumOccur": "1"
            },
            {
               "ID": "BP12",
               "NumOccur": "1"
            },
            {
               "ID": "IT01",
               "NumOccur": "2"
            },
            {
               "ID": "BP16",
               "NumOccur": "1"
            },
            {
               "ID": "BP32",
               "NumOccur": "1"
            },
            {
               "ID": "BP26",
               "NumOccur": "1"
            },
            {
               "ID": "BP48",
               "NumOccur": "1"
            },
            {
               "ID": "BP27",
               "NumOccur": "1"
            },
            {
               "ID": "BP08",
               "NumOccur": "1"
            }
         ]
      },
      "LNameInfo": {
         "LNameNum": "1",
         "NumPsgrs": "1",
         "NameType": [],
         "LName": "DOE"
      },
      "FNameInfo": {
         "PsgrNum": "1",
         "AbsNameNum": "1",
         "FName": "VERA"
      },
      "AirSeg": [
         {
            "SegNum": "1",
            "Status": "TK",
            "Dt": "20160502",
            "DayChg": "00",
            "AirV": "EY",
            "NumPsgrs": "1",
            "StartAirp": "AUH",
            "EndAirp": "XNB",
            "StartTm": "100",
            "EndTm": "220",
            "BIC": "K",
            "FltNum": "5412",
            "OpSuf": [],
            "COG": "N",
            "TklessInd": "N",
            "ConxInd": "N",
            "FltFlownInd": "N",
            "MarriageNum": [],
            "SellType": "O",
            "StopoverIgnoreInd": [],
            "TDSValidateInd": "N",
            "NonBillingInd": [],
            "PrevStatusCode": "HK",
            "ScheduleValidationInd": [],
            "VndLocInd": "*"
         },
         {
            "SegNum": "2",
            "Status": "TK",
            "Dt": "20160518",
            "DayChg": "00",
            "AirV": "EY",
            "NumPsgrs": "1",
            "StartAirp": "XNB",
            "EndAirp": "AUH",
            "StartTm": "455",
            "EndTm": "615",
            "BIC": "K",
            "FltNum": "5411",
            "OpSuf": [],
            "COG": "N",
            "TklessInd": "N",
            "ConxInd": "N",
            "FltFlownInd": "N",
            "MarriageNum": [],
            "SellType": "O",
            "StopoverIgnoreInd": [],
            "TDSValidateInd": "N",
            "NonBillingInd": [],
            "PrevStatusCode": "HK",
            "ScheduleValidationInd": [],
            "VndLocInd": "*"
         }
      ],
      "PhoneInfo": {
         "PhoneFldNum": "1",
         "Pt": "NBO",
         "Type": "A",
         "Phone": "DEWCIS SOLUTIONS REF SIDIKA"
      },
      "TkArrangement": [],
      "GenRmkInfo": {
         "GenRmkNum": "1",
         "CreationDt": "20160309",
         "CreationTm": "1259",
         "GenlRmkQual": "GS",
         "GenRmk": "TRANSFER/BUNSON/NBO CENTRE/NBO/09MAR/0359/1530"
      },
      "GenPNRInfo": {
         "FileAddr": "E0BDB1A8",
         "CodeCheck": "4F",
         "RecLoc": "37H3WC",
         "Ver": "6",
         "OwningCRS": "1G",
         "OwningAgncyName": "DEWCIS SOLUTIONS LTD",
         "OwningAgncyPCC": "63KN",
         "CreationDt": "20160109",
         "CreatingAgntSignOn": "63KNPN",
         "CreatingAgntDuty": "AG",
         "CreatingAgncyIATANum": "99999992",
         "OrigBkLocn": "NBOOU",
         "SATONum": [],
         "PTAInd": "N",
         "InUseInd": "N",
         "SimultaneousUpdInd": [],
         "BorrowedInd": "N",
         "GlobInd": "N",
         "ReadOnlyInd": "N",
         "FareDataExistsInd": "N",
         "PastDtQuickInd": "N",
         "CurAgncyPCC": "77QN",
         "QInd": "Y",
         "TkNumExistInd": "N",
         "IMUdataexists": "N",
         "ETkDataExistInd": "Y",
         "CurDtStamp": "20160503",
         "CurTmStamp": "092227",
         "CurAgntSONID": "MENG1RO",
         "TravInsuranceInd": [],
         "PNRBFTicketedInd": "N",
         "ZeppelinAgncyInd": "N",
         "AgncyAutoServiceInd": "N",
         "AgncyAutoNotifyInd": "N",
         "ZeppelinPNRInd": "N",
         "PNRAutoServiceInd": "N",
         "PNRNotifyInd": "N",
         "SuperPNRInd": "N",
         "PNRBFPurgeDt": "20160520",
         "PNRBFChangeInd": "N",
         "MCODataExists": "N",
         "OrigRcvdField": "M",
         "IntContExists": "N",
         "AllDataAllTime": "N"
      },
      "VndRecLocs": {
         "RecLocInfoAry": {
            "RecLocInfo": {
               "TmStamp": "941",
               "DtStamp": "20160109",
               "Vnd": "EY",
               "RecLoc": "HMMZTD"
            }
         }
      }
   },
   "SessionInfo": {
      "ErrorCode": "0003",
      "AreaInfoResp": {
         "Sys": "1G",
         "Processor": "D",
         "GrpModeActivatedInd": "N",
         "AAAAreaAry": [
            {
               "AAAArea": "A",
               "ActiveInd": "Y",
               "AAACity": "NBO",
               "AAADept": "NT",
               "SONCity": "77QN",
               "SONDept": [],
               "AgntID": "MENG1R",
               "ChkDigit": [],
               "AgntInitials": "RO",
               "Duty": "AG",
               "AgncyPCC": [],
               "DomMode": [],
               "IntlMode": [],
               "PNRDataInd": "Y",
               "PNRName": "DOE/VERA",
               "GrpModeActiveInd": [],
               "GrpModeDutyCode": [],
               "GrpModePCC": [],
               "GrpModeDataInd": [],
               "GrpModeName": []
            },
            {
               "AAAArea": "B",
               "ActiveInd": "A",
               "AAACity": "NBO",
               "AAADept": "NT",
               "SONCity": "77QN",
               "SONDept": [],
               "AgntID": "MENG1R",
               "ChkDigit": [],
               "AgntInitials": [],
               "Duty": [],
               "AgncyPCC": [],
               "DomMode": [],
               "IntlMode": [],
               "PNRDataInd": "N",
               "PNRName": [],
               "GrpModeActiveInd": [],
               "GrpModeDutyCode": [],
               "GrpModePCC": [],
               "GrpModeDataInd": [],
               "GrpModeName": []
            },
            {
               "AAAArea": "C",
               "ActiveInd": "A",
               "AAACity": "NBO",
               "AAADept": "NT",
               "SONCity": "77QN",
               "SONDept": [],
               "AgntID": "MENG1R",
               "ChkDigit": [],
               "AgntInitials": [],
               "Duty": [],
               "AgncyPCC": [],
               "DomMode": [],
               "IntlMode": [],
               "PNRDataInd": "N",
               "PNRName": [],
               "GrpModeActiveInd": [],
               "GrpModeDutyCode": [],
               "GrpModePCC": [],
               "GrpModeDataInd": [],
               "GrpModeName": []
            },
            {
               "AAAArea": "D",
               "ActiveInd": "A",
               "AAACity": "NBO",
               "AAADept": "NT",
               "SONCity": "77QN",
               "SONDept": [],
               "AgntID": "MENG1R",
               "ChkDigit": [],
               "AgntInitials": [],
               "Duty": [],
               "AgncyPCC": [],
               "DomMode": [],
               "IntlMode": [],
               "PNRDataInd": "N",
               "PNRName": [],
               "GrpModeActiveInd": [],
               "GrpModeDutyCode": [],
               "GrpModePCC": [],
               "GrpModeDataInd": [],
               "GrpModeName": []
            },
            {
               "AAAArea": "E",
               "ActiveInd": "A",
               "AAACity": "NBO",
               "AAADept": "NT",
               "SONCity": "77QN",
               "SONDept": [],
               "AgntID": "MENG1R",
               "ChkDigit": [],
               "AgntInitials": [],
               "Duty": [],
               "AgncyPCC": [],
               "DomMode": [],
               "IntlMode": [],
               "PNRDataInd": "N",
               "PNRName": [],
               "GrpModeActiveInd": [],
               "GrpModeDutyCode": [],
               "GrpModePCC": [],
               "GrpModeDataInd": [],
               "GrpModeName": []
            }
         ]
      }
   },
   "CorporateDataStore": {
      "ErrorCode": "0003",
      "ErrText": {
         "Err": "001006",
         "KlrInErr": [],
         "InsertedTextAry": [],
         "Text": "TDS ERROR-INVALID USER"
      }
   }
}
