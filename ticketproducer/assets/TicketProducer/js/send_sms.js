function sendMessage(){	
	var phone_num = document.getElementById("txt_phone_num").value;
	var message = document.getElementById("txt_message").value;
	
	if (phone_num == "") {
		document.getElementById("txt_phone_num").focus();
		setResponse("error", "Phone Number Cannot Be Blank");
		return false;
	}
	if (message == "") {
		document.getElementById("txt_message").focus();
		setResponse("error", "Please Type A Message");
		return false;
	}
	if (message.substring(0,7) == "NO B.F.") {
		document.getElementById("txt_message").focus();
		setResponse("error", "No PNR Set");
		return false;
	}
	
	else{
		//alert("ok to send Message");
		var loading = "loading_gif";
		setLoading(loading, true);
		setResponse("loading",  "Sending Message....");	
		var url ="process_request.jsp";
		var parameterString = "tag=post_sms&" + "phone_num=" + phone_num + "&message=" + message;
		
		
		
		//================================================================
		var xmlHttp = undefined;
		if (window.ActiveXObject){
			try {    xmlHttp = new ActiveXObject("MSXML2.XMLHTTP");
			} catch (othermicrosoft){
			try {  xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {}
			}
		}		
		if (xmlHttp == undefined && window.XMLHttpRequest) {	xmlHttp = new XMLHttpRequest();	} 		
		if (xmlHttp != undefined) {	
			
			xmlHttp.open("POST", url, true);
			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
					var res = xmlHttp.responseText;					
					if(res == 1){// exists
						setResponse("success", "Message Sent Successfully");
						document.getElementById("txt_phone_num").value = "";
						document.getElementById("txt_message").value = "";
						setLoading(loading, false);
					}else{
						setResponse("error", res);
						setLoading(loading, false);
					}					
				}				
			} 
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length",parameterString.length);
			xmlHttp.setRequestHeader("Connection", "close");
			xmlHttp.send(parameterString);
		}	
		//================================================================
	}
	
	
}