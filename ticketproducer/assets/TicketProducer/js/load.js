function showloading(){
    var loadnames = "<div class='panel panel-primary'>"
                       +"<div class='panel-heading'>Loading Passengers...</div>"
                      "</div>";
    var loadflights = "<div class='panel panel-primary'>"
                     +"<div class='panel-heading'>Loading Flights"
                     "</div>"
                    "</div>";
    var loadtickets = "<div class='panel panel-primary'>"
                    +"<div class='panel-heading'>Loading Tickets"
                    "</div>"
                    "</div>";
    $('#namestable').html(loadnames);
    $('#segmentdata').html(loadflights);
    $('#ticketdata').html(loadtickets);
}
