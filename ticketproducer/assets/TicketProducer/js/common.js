var TE = new ActiveXObject("DAT32COM.TERMINALEMULATION");

$("#reload").click(function(){
    location.reload();
});

$("#reload2").click(function(){
    location = window.location.href;
});

$("#reload3").click(function(){
    location = window.location.href;
});

var fare_xml = " ";
function GetRawFare(){
	//setLoading("loading_gif", true);
	try{
		var Viewpoint = new ActiveXObject("Viewpoint.ViewpointSrv");
		//setLoading("loading_gif", false);
		//setResponse("success",  "Viewpoint Loaded");
	}catch(e){
		//setLoading("loading_gif", false);
		//setResponse("error",  "Please Use Galileo Desktop To Enjoy This Service");
		return;
	}
	Viewpoint.RetrieveCurrentPNR();
	var CurrentFareQuote = Viewpoint.GetStoredFare();
	return CurrentFareQuote;
}
fare_xml = GetRawFare();

function GetRawPNR(){
	//setLoading("loading_gif", true);
	try{
		var VP = new ActiveXObject("Viewpoint.ViewpointSrv");
	}catch(e){
		//setLoading("loading_gif", false);
		//setResponse("error",  "Please Use Galileo Desktop To Enjoy This Service");
	}
	VP.RetrieveCurrentPNR();
	var PNR = VP.GetPNR();
	return PNR;
}
var pnr_xml  = GetRawPNR();



function ProcessPNR() {
	var loading = "loading_gif";
    var loadnames = "<div class='panel panel-primary'>"
                       +"<div class='panel-heading'>Loading Passengers...</div>"
                      "</div>";
    var loadflights = "<div class='panel panel-primary'>"
                     +"<div class='panel-heading'>Loading Flights"
                     "</div>"
                    "</div>";
    var loadtickets = "<div class='panel panel-primary'>"
                      +"<div class='panel-heading'>Loading Tickets"
                      "</div>"
                      "</div>";

    $('#namestable').html(loadnames);
    $('#segmentdata').html(loadflights);
    $('#ticketdata').html(loadtickets);

	$.post('processpnrtosegments',{pnr_xml:pnr_xml}, function(data){
		if(data.success == 1){

            var segtable = "";
            for(var segnum=0;segnum<data.segdata.length;segnum++){
                var flights  =  "<label class='mt-checkbox mt-checkbox-outline'>"
                                    + " "+data.segdata[segnum]['AirV']+" "
                                    + " "+data.segdata[segnum]['Class']+" "
                                    //+ " "+data.segdata[segnum]['Dt']+" "
                                    + " "+data.segdata[segnum]['StartAirp']+"  "
                                    + " "+data.segdata[segnum]['StartTm']+"  <b>to</b>  "
                                    + " "+data.segdata[segnum]['EndAirp']+" "
                                    + " "+data.segdata[segnum]['EndTm']+" "
                                    +"<input type='checkbox' name="+(segnum+1)+"  id="+(segnum+1)+"/>"
                                    +"<span></span>"
                                +"</label>";
                segtable += flights;

            }
            $('#segmentdata').html(segtable);
            //document.write(segtable);
            //start loading the passenger namedets
            var namesTable = "";
            for(var iname=0;iname<data.namedets.length;iname++){
                var names  =  "<label class='mt-checkbox mt-checkbox-outline'>"
                                    + " "+data.namedets[iname]['FName']+"        "
                                    + " "+data.namedets[iname]['LName']+"   "
                                    +"<input type='checkbox' name="+(iname+1)+" id="+(iname+1)+" />"
                                    +"<span></span>"
                                +"</label>";
                namesTable += names;
            }
            console.log("namesTable " + namesTable);
            $('#namestable').html(namesTable);

            var ticketData = "";
            for(var tic=0;tic<data.ticketnumbers.length;tic++){
                var tickets  =  "<label class='mt-checkbox mt-checkbox-outline'>"
                                    + " "+data.ticketnumbers[tic]['Text']+"        "
                                    +"<input type='checkbox' name="+(tic+1)+" id="+(tic+1)+" />"
                                    +"<span></span>"
                                +"</label>";
                ticketData += tickets;
            }
            console.log("ticketData " + ticketData);
            $('#ticketdata').html(ticketData);


            if(namesTable == ""){
                var nopass = "<div class='panel panel-primary'>"
                                 +"<div class='panel-heading'>No Passanger Found.."
                                 +"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                 +"<button id='reload'>Reload...</button>"
                                 "</div>"
                                "</div>";
                $('#namestable').html(nopass);

            }

            if(segtable == ""){
                var noflight = "<div class='panel panel-primary'>"
                                 +"<div class='panel-heading'>No Flight Found.."
                                 +"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                 +"<button id='reload2'>Reload...</button>"
                                 "</div>"
                                "</div>";
                $('#segmentdata').html(noflight);

            }
            if(ticketData == ""){
                var noticket = "<div class='panel panel-primary'>"
                                 +"<div class='panel-heading'>No Tickets Found.."
                                 +"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                 +"<button id='reload3'>Reload...</button>"
                                 "</div>"
                                "</div>";
                $('#ticketdata').html(noticket);

            }
		}else if(data.success == 0){
			//setResponse("error", data.message);
			//setLoading(loading, false);
		}
	},'JSON');
}
function goToPage(page){
	location.replace(page);
}




function GetRawPNR(){
	//setLoading("loading_gif", true);
	try{
		var VP = new ActiveXObject("Viewpoint.ViewpointSrv");
	}catch(e){
		//setLoading("loading_gif", false);
		//setResponse("error",  "Please Use Galileo Desktop To Enjoy This Service");
	}
	VP.RetrieveCurrentPNR();
	var PNR = VP.GetPNR();
	return PNR;
}


function GetSignOn(){

	setLoading("loading_gif", true);
	try{
		var Viewpoint = new ActiveXObject("Viewpoint.ViewpointSrv");
	}catch(e){
		//setLoading("loading_gif", false);
		//setResponse("error",  "Please Use Galileo Desktop To Enjoy This Service");
		return;
	}


	try{
		Viewpoint.RetrieveCurrentPNR();
		var son = Viewpoint.GETUSERSON(0);
	}catch(ex){
		setResponse("error",  "SON ERROR ");
		return false;
	}

	var arrayOfLines = DisplayTE('QCA').match(/[^\r\n]+/g);
	var str = arrayOfLines[0];
	var pos1 = str.indexOf(" ") + 1;
	var pos2 = str.indexOf(" ",pos1);
	var pcc = str.substring(pos1,pos2);

	return {"son":son, "pcc":pcc};
}

function DisplayTE(gcmd) {
	//console.log('Writing to Free Field');
	//var TE = new ActiveXObject("DAT32COM.TERMINALEMULATION");
	var command = "<FORMAT>" + gcmd + "</FORMAT>";
	TE.MakeEntry(command);
	while (TE.More == true) {
		TE.GetMore(true, false);
	}

	var rl = TE.NumResponseLines;
	//alert(rl);
	var result = "";
	var i;
	for (i = 0; i < rl; i++) {
		result = result + TE.ResponseLine(i).replace("<CARRIAGE_RETURN/>", "").replace("<SOM/>", "").replace("<TABSTOP/>", "\t") + "\n";
	}
	//console.log('result : ' + result);
	return result;
}



function createItinerary(){
    //getting the checkboxes that are checked on the form
    var selected = '{';
    $('#namestable input:checked').each(function() {
        //selected += '"'+$(this).attr('name')+'":"'+$(this).attr('name')+'",';
        selected += ""+$(this).attr('name')+",";
    });
    var pax = selected.substring(0,(selected.length-1)).trim();
    pax += '}';
    //var selectedParsed = JSON.parse(selected2.trim());

    var selectedsegnum = '{';
    $('#segmentdata input:checked').each(function() {
        selectedsegnum += ""+$(this).attr('name')+",";
    });
    var seg = selectedsegnum.substring(0,(selectedsegnum.length-1)).trim();
    seg += '}';
    //document.write(seg);
    var printSeparate = "";


    //ri remarks
    var remarks = $("#checkbox").is(":checked");
    var rmks = "rmk";
    if(remarks == true){
        rmks = "remarksTrue";
    }
    if(remarks == false){
        rmks = "remarksFalse";
    }

    var fareRmk = $("#fare").is(":checked");
    var fare = "fare";
    if(fareRmk == true){
        fare = "fareTrue";
    }
    if(fareRmk == false){
        fare = "fareFalse";
    }
    //include segments and pax in {} below
	$.post('processpnrsegs',{pax:pax,seg:seg,remarks:rmks,fare:fare}, function(data){
			$('#reportdata').val(data.jreport);
	},'JSON');
}

function createSummary(){
    if($("#opt1").is(':checked')){
        printSeparate = "print";
    }
     else{
         printSeparate = "dontprint";
     }
     //getting the pax
     var selected = '{';
     $('#ticketdata input:checked').each(function() {
         //selected += '"'+$(this).attr('name')+'":"'+$(this).attr('name')+'",';
         selected += ""+$(this).attr('name')+",";
     });
     var pax = selected.substring(0,(selected.length-1)).trim();
     pax += '}';



    //summary report
    $.post('processpnrsegs2',{pax:pax,printSeparate:printSeparate}, function(data){
        var datastr = (data.jreport);
        $('#reportdata2').val(datastr);
	},'JSON');
}
