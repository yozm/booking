/*
 *Last Update 23 jul 2014
 */

$(document).ready(function() {
	loadMessages(false);
	$(document).prop('title', $('#tx_org').val());
} );


function loadMessages(sent){
	var loading = "loading_gif";
	setLoading(loading, true);
	setResponse("loading",  "Retrieveing Messages");
	
	$.post('processrequest',{tag:'outbox',sent:sent}, function(data){
		if(data.success == 1){
			var sOut = '<table class="table" id="tbl_outbox">';
			sOut += '<thead><tr><th>#</th>';
			sOut += '<th>Phone</th>';
			sOut += '<th>Sent</th>';
			sOut += '<th>Date Sent</th>';
			sOut += '<th>Message</th>';			
			sOut += '</tr></thead>';
			sOut += '<tbody>';
			
			var things = data.array;
			for(var i = 0; i < things.length; i++){
				var phone = things[i]['number'];
				var message = things[i]['message']; //
				var sent = things[i]['sent'];
				var date = things[i]['date'];
				var type = "";
				if(sent== 'No'){
					type = 'info';
				}else if(sent == 'Yes'){
					type = 'success';
				}
				
				sOut += '<tr class="' + type + '">' +
							'<td >' + (i+1) +'</td>'+
							'<td >'+ phone +'</td>'+
							'<td >'+ sent +'</td>'+
							'<td style="width:10%">'+ date +'</td>'+
							'<td >'+ message +'</td>'+
							
						'</tr>'; 
			}
			sOut += '</tbody></table>';
			
			$("#outbox_sms").html(sOut);
			
			//http://www.jqueryis.com/twitter-bootstrap-table-pagination-datatables/
			$('#tbl_outbox').dataTable( {
			    "bSort": true,      // Disable sorting
				"iDisplayLength": 5,   //records per page
				 	"sDom": "t<'row'<'col-md-6'i><'col-md-6'p>>",
					"sPaginationType": "bootstrap"
			} );
			
			
			setLoading(loading, false);
			setResponse("success", data.message);
			
			
		}else if(data.success == 0){
			setLoading(loading, false);
			setResponse("info", data.message);
			$("#outbox_sms").html(data.message);
		}		
	},'JSON');
		
	
	
}
