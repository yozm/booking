function getUserDetails(){
	console.log("validating here");
	var loading = "loading_gif";
	setLoading(loading,true);
	var user_details = GetSignOn();
	var son = user_details['son'];
	var pcc = user_details['pcc'];
	var app = $("#tx_app").val();
	if(pcc == null || pcc == ""){
		setResponse("error",  "Error Getting Pcc");
		setLoading(loading,false);
		return false;
	}
	if(son == null || son == ""){
		setResponse("error",  "Error Getting SON");
		setLoading(loading, false);
		return false;
	}else{
		setLoading(loading, true);
		setResponse("loading",  "Validating Your Account....");
		$.post('validating', { tag:'authenticate',pcc:pcc, son:son,app:app } , function(data){
			if(data.success == 1){// exists
				setResponse("success", data.message);
				location.replace('home.jsp');
				setLoading(loading, false);
			}else{
				setResponse("error", data.message);
				setLoading(loading, false);
			}
		},"JSON");
	}
}


function setResponse(type, message){
    var $btnRetry = $('#btnRetry');
	var $loader = $('#loading_gif');
    var $lblResponse = $('#lblResponse');
	var $response = $('#response');

    if(type == 'error'){
        $loader.addClass('hidden');
        $lblResponse.html(message);
        $lblResponse.addClass('bg-danger');
        $btnRetry.removeClass('hidden');
    }else if(type == 'success'){
        $loader.addClass('hidden');
        $lblResponse.html(message);
        $lblResponse.addClass('bg-success');
        $btnRetry.addClass('hidden');

    }else if(type == 'info'){
        $loader.removeClass('hidden');
        $lblResponse.html(message);
        $lblResponse.addClass('bg-info');
        $btnRetry.addClass('hidden');
    }else if(type == 'loading'){
		$response.html(message);
		document.getElementById('response').style.color = '#009DDC';

	}
}

function setLoading(id, visible) {
    var img = document.getElementById(id);
    img.style.visibility = (visible ? 'visible' : 'hidden');
}
