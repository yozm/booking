<%
	if(session.getAttribute( "pcc" ) == null && session.getAttribute( "son" ) == null && session.getAttribute("jsondata") == null){
		response.sendRedirect( response.encodeRedirectURL("index.jsp"));
	}
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:set var="appRoot" value="TicketProducer" />
<c:set var="appName" value="Ticket Producer" />
<%response.addHeader("Cache-Control","no-cache");%>
<%response.addHeader("Expires","-1");%>
<%response.addHeader("Pragma","no-cache");%>
<%
//System.out.println("b file from C# " + session.getAttribute("bookingFile"));
%>


<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" onload="showloading()">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Travelport Apps | TicketProducer....</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Travelport Kenya Travdoc Itinerary Viewer" name="description" />
        <meta content="Dew CIS Solutions" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="assets/global/plugins/font-oswald/oswald.css" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="assets/global/plugins/font-opensans/opensans.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/pages/css/doc-2.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout5/css/custom.css" rel="stylesheet" type="text/css" />
		<link href="assets/layouts/layout5/css/print.css" rel="stylesheet" type="text/css" media="print" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body onload="showloading();ProcessPNR()" class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        <div class="clearfix navbar-fixed-top">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
                            <!-- BEGIN LOGO -->
                            <a id="index" class="page-logo" href="index.html">
                                <img src="assets/layouts/layout5/img/logo.png" alt="Logo"> </a>
                            <!-- END LOGO -->
                            <!-- BEGIN TOPBAR ACTIONS -->
                            <div class="topbar-actions">


                                <!-- BEGIN USER PROFILE -->
                                <div class="btn-group-img btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span>Welcome, <%=session.getAttribute( "name")%> </span>
                                        <!--<img src="assets/layouts/layout5/img/avatar1.jpg" alt=""> </button>-->
                                </div>
                                <!-- END USER PROFILE -->
                                <!-- BEGIN GROUP ACTIONS -->
                                <div class="btn-group-red btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="fa fa-gear"></i>
                                    </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li class="active">
                                            <a href="#">New Post</a>
                                        </li>
                                        <li>
                                            <a href="#">New Comment</a>
                                        </li>
                                        <li>
                                            <a href="#">Share</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="#">Comments
                                                <span class="badge badge-success">4</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">Feedbacks
                                                <span class="badge badge-danger">2</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- END GROUP INFORMATION -->
                            </div>
                            <!-- END TOPBAR ACTIONS -->
                        </div>
                    </div>
                    <!--/container-->
                </nav>
            </header>
            <!-- END HEADER -->
            <div class="container">
                <div class="page-content">

                    <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
                    <div class="page-content-container">
                        <div class="page-content-row">

                            <div class="page-content-col travdoc">
                                <!-- BEGIN PAGE BASE CONTENT -->
								<div class="row">
								    <div class="col-md-12">
								        <div>
								            <div class="portlet light bordered">
								                <div class="portlet-title">
								                    <div class="caption">
								                        <i class="icon-plane  font-blue-sharp"></i>
								                        <span class="caption-subject font-blue-sharp bold uppercase">Travdoc Itinerary Maker</span>
								                    </div>
								                </div>
								                <div class="portlet-body form">
													<!-- BEGIN FORM-->
													<form action="#" class="horizontal-form">
													    <div class="form-body">
													        <h4 class="form-section">Passenger(s)</h4>
													        <div class="row">
													            <div class="col-md-6">
													                <div class="form-group">
													                    <label class="control-label">Passenger(s)</label>
																		<div class="mt-checkbox-list" id="namestable">
																			<!--
																		    <label class="mt-checkbox mt-checkbox-outline"> KQ600 V 2APR NBO 0630 MBA 0730 HS
																		        <input type="checkbox" value="SEG1" name="SEG1" />
																		        <span></span>
																		    </label>
																		    <label class="mt-checkbox mt-checkbox-outline"> KQ601 N 7APR NBO 1630 MBA 1730 HS
																		        <input type="checkbox" value="SEG2" name="SEG2" />
																		        <span></span>
																		    </label>
																		    -->

																		</div>


																			   <!--
																		        <option>P1 - Jane Doe</option>
																		        <option>P2 - James Doe</option>
																		        <option>P3 - Anne Doe</option>
																		        <option>P4 - Mike Doe</option>
																			    -->
													                </div>
													            </div>
													            <!--/span-->
													            <div class="col-md-6">
													                <div class="form-group">
													                    <label class="control-label">Flights</label>
																		<div class="mt-checkbox-list" id="segmentdata">
																			<!--
																		    <label class="mt-checkbox mt-checkbox-outline"> KQ600 V 2APR NBO 0630 MBA 0730 HS
																		        <input type="checkbox" value="SEG1" name="SEG1" />
																		        <span></span>
																		    </label>
																		    <label class="mt-checkbox mt-checkbox-outline"> KQ601 N 7APR NBO 1630 MBA 1730 HS
																		        <input type="checkbox" value="SEG2" name="SEG2" />
																		        <span></span>
																		    </label>
																		    -->

																		</div>
													                </div>
													            </div>
													            <!--/span-->
													        </div>
													        <!--/row-->
													        <h4 class="form-section">Printing Options</h4>
													        <div class="row">
													            <div class="col-md-12">
													                <div class="form-group">
																		<div class="mt-checkbox-list" class="printingoptions">
																		    <label class="mt-checkbox mt-checkbox-outline"> Print separate Itineraries for each selected passenger
																		        <input type="checkbox"  name="opt1" id="opt1"/>
																		        <span></span>
																		    </label>
																		    <label class="mt-checkbox mt-checkbox-outline"> Include general RI entries
																		        <input type="checkbox" name="remarks" id="checkbox"  value="remarksHtml"/>
																		        <span></span>
																		    </label>
																			<label class="mt-checkbox mt-checkbox-outline"> Include Fare
																		        <input type="checkbox"  name="fare" id="fare"  value="includefare" />
																		        <span></span>
																		    </label>
																		</div>
													                </div>
													        </div>
													    </div>
													</form>
													<form  action="reportcontroller" method="post">
														<div class="form-actions right">
															<input type="text" name="reportdata" id="reportdata">
															<input type="text" name="item" value="itinerary">
																<div class="col-md-12">
																	<div class="col-md-3"></div>
																	<div class="col-md-3">
																		<select id="reportType" class="btn default" name="reportType">
																			<option value="pdf" selected>pdf</option>
																			<option value="docx">ms-word</option>
																		</select>
																	</div>
																	<div class="col-md-3">
																		<button type="submit" class="btn blue" onmouseover="createItinerary()">
																            <i class="fa fa-check"></i>Itinerary</button>
																	</div>
																	<div class="col-md-3">
																	</div>
																</div>
													    </div>
													</form>
														<form action="#" class="horizontal-form">
																<div class="form-body">
																		<h4 class="form-section">Eticket(s)</h4>
																		<div class="row">
																				<div class="col-md-6">
																						<div class="form-group">
																								<label class="control-label">Eticket(s)</label>
																			<div class="mt-checkbox-list" id="ticketdata">
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</form>

														<form  action="reportcontroller" method="post">
															<div class="form-actions right">
																<!-- make etic button incative if reportdata is null -->
																	<div class="col-md-12">
																		<div class="col-md-3">
																			<input type="text" name="reportdata" id="reportdata2">
																			<input type="text" name="item" value="summarydata">
																		</div>
																		<div class="col-md-3">
																			<select id="reportType" class="btn default" name="reportType">
																				<option value="pdf" selected>pdf</option>
																				<option value="docx">ms-word</option>
																			</select>
																		</div>
																		<div class="col-md-3">
																			<button type="submit" class="btn blue" onmouseover="createSummary()">
																	            <i class="fa fa-check"></i>E-Ticket</button>
																		</div>
																	</div>
														    </div>
														</form>
													<!-- END FORM-->
								                </div>
								            </div>
								        </div>
								    </div>
								</div>
                                <!-- END PAGE BASE CONTENT -->
                            </div>
                        </div>
                    </div>
                    <!-- END SIDEBAR CONTENT LAYOUT -->
                </div>
                <!-- BEGIN FOOTER -->
                <p class="copyright"> 2016 &copy; Travelport Kenya TicketProducer. &nbsp;|&nbsp; Developed by
                    <a target="_blank" href="http://www.dewcis.com">Dew CIS Solutions</a>
                </p>
                <a href="#index" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>
                <!-- END FOOTER -->
            </div>
        </div>
        <!-- END CONTAINER -->


        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<script src="assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- others -->
        <script src="${contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="${contextPath}/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="${contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="${contextPath}/assets/${appRoot}/js/load.js" ></script>
        <script src="${contextPath}/assets/${appRoot}/js/common.js" ></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
