import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.io.IOException;
import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.*;
import org.baraza.DB.BDB;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

@SuppressWarnings("unchecked")
@WebServlet("/jreports")
public class DataExportServlet extends HttpServlet{
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		HttpSession session = request.getSession(false);
        try{
        String reportName = (String)session.getAttribute("reportname");
		String reportTitle = reportName;
        reportName = "/travdoc_jreport.jasper";
        ServletContext context = getServletContext();
        String reportPath = context.getRealPath("/reports");
        System.out.println("reportPath: " + reportPath);
        File reportFile = new File(reportPath + reportName);
        if (!reportFile.exists()) {
            System.out.println("Report access error");
            return;
        }
		String reportdata = request.getParameter("reportdata");
        String reportType  = request.getParameter("reportType");
        //System.out.println("reportData " + reportdata);
        if(reportType == null) reportType = "pdf";
        String viewPeriod  = request.getParameter("viewPeriod");
        System.out.println(viewPeriod);

        Map<String,Object> params = new HashMap<String,Object>();
        //params.put("reportType", reportType);
        //params.put("viewPeriod", viewPeriod);

        final OutputStream outStream = response.getOutputStream();

        String usageData = reportdata;
        System.out.println("BASE JSON " + usageData);

        InputStream usageStream = new ByteArrayInputStream(usageData.getBytes(StandardCharsets.UTF_8));
        JasperReport jasperReport = (JasperReport)JRLoader.loadObjectFromFile(reportFile.getPath());
        JRDataSource ds = new JsonDataSource(usageStream);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);

        //System.out.println("reached here " + usageData);
        if(reportType.equals("pdf")) {
            response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=" + reportTitle + ".pdf");
            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
        } else if(reportType.equals("xls")) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            JRXlsExporter exporterXls = new JRXlsExporter ();
            exporterXls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXls.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
            exporterXls.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
            exporterXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
            exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
            exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
            exporterXls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
            exporterXls.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, "report.xls");
            exporterXls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
            exporterXls.exportReport();

            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=" + reportTitle + ".xls");

            outStream.write(byteArrayOutputStream.toByteArray());
            outStream.flush();
            outStream.close();
        } else if(reportType.equals("docx")) {
            response.setContentType("application/vnd.ms-word");
            response.setHeader("Content-Disposition", "attachment;filename=" + reportTitle + ".docx");
            JRDocxExporter exporter = new JRDocxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outStream);
            exporter.exportReport();
        }
     }
        //end of report
    catch(Exception e){}

    }

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{

    }
}
