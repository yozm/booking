import java.io.IOException;
import java.io.PrintWriter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

//import java.text.ParseException;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.annotation.WebServlet;


import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Enumeration;

import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List;
import java.util.Arrays;

import org.baraza.xml.BXML;
import org.baraza.xml.BElement;
import org.baraza.DB.BDB;
import org.baraza.DB.BQuery;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.ContentHandler;
import org.json.simple.parser.JSONParser;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import java.io.*;
import javax.servlet.*;

//tickdate

@WebServlet("/processpnrtosegments")
public class ProcessPNRXMLToSegments extends HttpServlet{
    static Logger log = Logger.getLogger(ProcessPNRXMLToSegments.class.getName());
    public static final String KEY_NO_MATCH = "NO_MATCH_FOUND";
    public static String dateFormat = "dd-MM-yyyy hh:mm";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
    @SuppressWarnings("unchecked")
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse rsp)    throws ServletException, IOException {
    //new code for processpnrsegs using C#
    JSONParser jsonParser=new JSONParser();
    HttpSession session = req.getSession(false);

    BDB db = new BDB("java:/comp/env/jdbc/database");
    int markup1 = 0;

    rsp.setContentType("application/json");
    PrintWriter out = rsp.getWriter();
    JSONObject jobj=new JSONObject();

    String issue_date = req.getParameter("issue_date");
    session.setAttribute("issue_date",issue_date);
    String xml_string = null;
    xml_string = req.getParameter("pnr_xml");
    //System.out.println("pnr xml: " + xml_string);

    String getItin_json = req.getParameter("getItin");
    String meal_json = req.getParameter("mealsdata");
    Map<String, String> lName = new HashMap<String, String>();
    Map<String, String> fName = new HashMap<String, String>();
    Map<String, String> phonInfo = new HashMap<String, String>();
        JSONArray airSegments = new JSONArray();
        JSONArray itinRmks = new JSONArray();
        JSONArray genRmkInfo = new JSONArray();
        JSONArray weather_array = new JSONArray();
        ArrayList<String> city = new ArrayList<String>();
       //adding other details
        JSONArray mealplan = new JSONArray();
        JSONArray ticketnumber = new JSONArray();
        JSONArray SeatAssignment = new JSONArray();
        JSONArray freqflierArray = new JSONArray();
        JSONArray phoneArray = new JSONArray();
        JSONArray lastNarray = new JSONArray();
        JSONArray firstNarray = new JSONArray();
        JSONArray fnamelname = new JSONArray();
        JSONArray vendorRkm = new JSONArray();
        JSONArray passRemarks = new JSONArray();
        JSONArray passengerDet = new JSONArray();
        JSONArray allPassDet = new JSONArray();
        JSONArray reclocator = new JSONArray();
        JSONArray VndRecLocsArr = new JSONArray();
        JSONArray tk_date = new JSONArray();
        ArrayList<String> ticstores = new ArrayList<String>();

        if(!xml_string.isEmpty() && xml_string != null){
            BElement root = new BXML(xml_string, true).getRoot().getElementByName("PNRBFRetrieve");
            for(BElement el : root.getElements()) {
                JSONObject seatpos = new JSONObject();
                JSONObject ticketObject = new JSONObject();
                JSONObject airSeg = new JSONObject();
                JSONObject mealobject = new JSONObject();
                JSONObject itinRmk = new JSONObject();
                JSONObject genRmk = new JSONObject();
                JSONObject freqflier = new JSONObject();
                JSONObject phoneObj = new JSONObject();
                JSONObject lnameOb = new JSONObject();
                JSONObject fnameobj = new JSONObject();
                JSONObject vremarks = new JSONObject();
                JSONObject pasrkms = new JSONObject();
                JSONObject locput = new JSONObject();
                JSONObject VndRecLocsObj = new JSONObject();
                JSONObject tk_dateObj = new JSONObject();



                String elName = el.getName();
                if(elName == null) elName = "";


                if(elName.equals("VndRecLocs")) {
                    for(BElement el2 : el.getElements()) {
                        String elname2 = el2.getName();
                        if(elname2 ==null) elname2 ="";
                        if(elname2.equals("RecLocInfoAry")){
                            for(BElement el3 : el2.getElements()) {
                                String elname3 = el3.getName();
                                if(elname3 == null) elname3 ="";
                                if(elname3.equals("RecLocInfo")){
                                    VndRecLocsObj.put("RecLoc",el3.getElementByName("RecLoc").getValue());
                                    VndRecLocsArr.add(VndRecLocsObj);
                                }
                            }
                        }
                    }
                }


                if(elName.equals("NonProgramaticSSR")) {
                    pasrkms.put("SSRText",el.getElementByName("SSRText").getValue());
                   passRemarks.add(pasrkms);
                }

                if(elName.equals("VndRmk")) {
                    vremarks.put("Rmk",el.getElementByName("Rmk").getValue());
                    vendorRkm.add(vremarks);
                }

                if(elName.equals("LNameInfo")) {
                    lName.put(el.getElementByName("LNameNum").getValue(), el.getElementByName("LName").getValue());
                    lnameOb.put("LName",el.getElementByName("LName").getValue());
                    lastNarray.add(lnameOb);
                }

                if(elName.equals("FNameInfo")) {
                    fName.put(el.getElementByName("AbsNameNum").getValue(), el.getElementByName("FName").getValue());
                    fnameobj.put("FName",el.getElementByName("FName").getValue());
                    fnameobj.put("AbsNameNum",el.getElementByName("AbsNameNum").getValue());
                    firstNarray.add(fnameobj);
                }


                if(elName.equals("ProgramaticSSR")) {
                        String myMealsCode = el.getElementByName("SSRCode").getValue();
                        for(BElement el2 : el.getElements()) {
                            String elname2 = el2.getName();
                            if(elname2 ==null) elname2 ="";
                            if(elname2.equals("AppliesToAry")){
                                for(BElement el3 : el2.getElements()) {
                                    String elname3 = el3.getName();
                                    if(elname3 == null) elname3 ="";
                                    if(elname3.equals("AppliesTo")){
                                        mealobject.put("LNameNum",el3.getElementByName("LNameNum").getValue());
                                        mealobject.put("PsgrNum",el3.getElementByName("PsgrNum").getValue());
                                        mealobject.put("AbsNameNum",el3.getElementByName("AbsNameNum").getValue());
                                    }
                                }
                            }
                        }
                    try{
                        String ssrquery = "select ssrname from specialservices where ssrcode='"+myMealsCode+"' LIMIT 1";
                        ResultSet resssr = db.readQuery(ssrquery);
                        while(resssr.next()){
                            mealobject.put("SSRCode",resssr.getString("ssrname"));
                        }
                    }
                    catch(SQLException sqle){
                        sqle.printStackTrace();
                    }
                     //mealobject.put("SSRCode",myMealsCode);
                      mealobject.put("Status",el.getElementByName("Status").getValue());
                      mealobject.put("SegNum",el.getElementByName("SegNum").getValue());
                      mealplan.add(mealobject);
              }


                if(elName.equals("ProgramaticSSRText")) {
                    String t_num = el.getElementByName("Text").getValue();
                    //t_num.substring(4,8) check if integer
                    if(t_num.length()== 15){
                      String t_number  = t_num.substring(0,t_num.length()-2);
                      if (!ticstores.contains(t_number)) {
                      ticketObject.put("Text",t_number);
                       ticstores.add(t_number);
                       ticketnumber.add(ticketObject);

                     }

                    }
                };



                if(elName.equals("FreqCustInfoEx")) {
                freqflier.put("FreqCustNum",el.getElementByName("FreqCustNum").getValue());
                 freqflier.put("LNameNum",el.getElementByName("LNameNum").getValue());
                 freqflierArray.add(freqflier);
                }
               //getting pnr data from xml and adding intinerary data from the booking file

                int index =0 ;
                if(elName.equals("AirSeg")) {
                    index++;
                try{
                    String carrier =  el.getElementByName("AirV").getValue().toString();
                    String carrierClass = el.getElementByName("BIC").getValue().toString();
                    ////.println("carrier:" + carrier + " Booking class: " + carrierClass);
                    String bclass = "class";
                    String bclassquery = "SELECT name FROM bookingclass WHERE class='"+carrierClass+"' "
                    + " AND   carrier = '"+carrier+"'   ";
                    //.println(bclassquery);
                    ResultSet res = db.readQuery(bclassquery);
                    while (res.next()){
                        bclass = res.getString("name");
                      ////.println("class: " + bclass);
                        airSeg.put("Class",bclass);
                        airSeg.put("cabin",carrierClass);
                    }
                    if(bclass == "class"){
                        String bclassquery2 = "SELECT name FROM bookingclass WHERE class='"+carrierClass+"' "
                        + " AND   carrier = 'OTHER'   ";
                        ////.println(bclassquery2);
                        ResultSet res2 = db.readQuery(bclassquery2);
                        while (res2.next()){
                            bclass = res2.getString("name");
                            ////.println("class: " + bclass);
                            airSeg.put("Class",bclass);
                            airSeg.put("cabin",carrierClass);
                        }
                    }
                res.close();
                }
                catch(SQLException sqe){
                    sqe.printStackTrace();
                }

                    String statusCode = el.getElementByName("Status").getValue();
                    String rawtime = el.getElementByName("StartTm").getValue();
                    if(rawtime.length()==3){
                        rawtime = "0"+rawtime;
                    }


                    int hours = Integer.parseInt(rawtime.substring(0,2));
                    String mins  = rawtime.substring(2,4);
                    int timeAmPm = 0;
                    String format = "";

                    if(hours < 12){
                        timeAmPm = hours;
                        format = "AM";
                    }
                    if(hours == 12){
                        timeAmPm = hours ;
                        format = "NOON";
                    }
                    if(hours == 00){
                        timeAmPm = hours ;
                        format = "MIDNIGHT";
                    }
                    if(hours > 12){
                        timeAmPm = hours - 12;
                        format = "PM";
                    }
                    String fulltime = Integer.toString(timeAmPm)+":"+mins+format;
                    //end time formatting
                    String rawtime2 = el.getElementByName("EndTm").getValue();
                    if(rawtime2.length()==3){
                        rawtime2 = "0"+rawtime2;
                    }
                    int hours2 = Integer.parseInt(rawtime2.substring(0,2));
                    String mins2  = rawtime2.substring(2,4);
                    int timeAmPm2 = 0;
                    String format2 = "";
                    if(hours2 < 12){
                        timeAmPm2 = hours2;
                        format2 = "AM";
                    }
                    if(hours2 == 12){
                        timeAmPm2 = hours2 ;
                        format2 = "NOON";
                    }
                    if(hours2 == 00){
                        timeAmPm2 = hours ;
                        format2 = "MIDNIGHT";
                    }
                    if(hours2 > 12){
                        timeAmPm2 = hours2 - 12;
                        format2 = "PM";
                    }
                    String fulltime2 = Integer.toString(timeAmPm2)+":"+mins2+format2;
                    airSeg.put("SegNum", el.getElementByName("SegNum").getValue());
                    airSeg.put("Dt", el.getElementByName("Dt").getValue());
                    airSeg.put("AirV", el.getElementByName("AirV").getValue());
                    airSeg.put("FltNum", el.getElementByName("FltNum").getValue());
                    airSeg.put("startcode", el.getElementByName("StartAirp").getValue());
                    airSeg.put("endcode", el.getElementByName("EndAirp").getValue());
                    airSeg.put("StartAirp", getCityName(el.getElementByName("StartAirp").getValue()));
                    airSeg.put("EndAirp", getCityName(el.getElementByName("EndAirp").getValue()));
                    airSeg.put("StartA", getAirportName(el.getElementByName("StartAirp").getValue()));
                    airSeg.put("EndA", getAirportName(el.getElementByName("EndAirp").getValue()));
                    String fromTo = el.getElementByName("StartAirp").getValue() + "-" +el.getElementByName("EndAirp").getValue();
                    airSeg.put("fromTo",fromTo);
                    airSeg.put("StartTm", fulltime);
                    airSeg.put("rawtime", rawtime);
                    airSeg.put("EndTm", fulltime2);
                    airSeg.put("Dt2",  el.getElementByName("Dt").getValue());
                    if(city.indexOf(getCityName(el.getElementByName("StartAirp").getValue())) == -1){
                        city.add(getCityName(el.getElementByName("StartAirp").getValue()));
                    }
                    if(city.indexOf(getCityName(el.getElementByName("EndAirp").getValue())) == -1){
                        city.add(getCityName(el.getElementByName("EndAirp").getValue()));
                    }
                    airSegments.add(airSeg);
                }


                if(elName.equals("GenPNRInfo")) {
                     locput.put("recordlocator",el.getElementByName("RecLoc").getValue());
                     reclocator.add(locput);
                }

        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Itinerary Remarks
                if(elName.equals("ItinRmk")){
                    itinRmk.put("itinRmk",el.getElementByName("Rmk").getValue());
                    itinRmks.add(itinRmk);
                }

                if(elName.equals("GenRmkInfo")) {
                    genRmk.put("GenRmk",el.getElementByName("GenRmk").getValue());
                    genRmkInfo.add(genRmk);
                }

                if(elName.equals("PhoneInfo")) {
                    phoneObj.put("Phone",el.getElementByName("Phone").getValue());
                    phoneArray.add(phoneObj);
                }
            }

            for(String mycity : city){
                JSONObject weatherObj = new JSONObject();
                if(mycity.indexOf(" ") >0){
                    mycity = mycity.replaceAll(" ","");
                }

                ////.println(mycity);
                String url = "http://api.openweathermap.org/data/2.5/forecast/daily?q="+mycity+"&mode=json&units=metric&cnt=5&appid=c2b25820ea45dd5f2bb3d42a33b6dd4a";
                try {
                    String genreJson = IOUtils.toString(new URL(url));
                    JSONObject genreJsonObject = (JSONObject) JSONValue.parseWithException(genreJson);
                    weatherObj.put("city",genreJsonObject.get("city"));
                    JSONArray genreArray = (JSONArray) genreJsonObject.get("list");
                    weatherObj.put("list",genreArray);
                    for(int i = 0; i< genreArray.size();i++ ){
                  JSONObject firstGenre = (JSONObject) genreArray.get(i);
                        String dt = firstGenre.get("dt").toString()+"000";
                        firstGenre.put("dt",ConvertDate(dt));
                        weatherObj.put("dt",ConvertDate(dt));
                    }
                } catch (IOException | ParseException e) {
                    e.printStackTrace();
                }
                weather_array.add(weatherObj);
            }


        //constructing the consultant
        JSONArray consultant = new JSONArray();
        String son = (String)session.getAttribute("son");
        String pcc = (String)session.getAttribute("pcc");
        String org_id = (String)session.getAttribute("org_id");
        String consultantQuery = "select entity_name,primary_email,"
        + "primary_telephone from entitys where son='"+son+"' "
        + "and org_id = '"+org_id+"'    limit 1";
        log.info(consultantQuery);
        try{
            ResultSet resCon = db.readQuery(consultantQuery);
            while(resCon.next()){
                JSONObject consPut = new JSONObject();
                if(resCon.getString("entity_name") != null) consPut.put("name", resCon.getString("entity_name"));
                   else  consPut.put("name","");
                if(resCon.getString("primary_telephone") != null) consPut.put("mobile", resCon.getString("primary_telephone"));
                   else  consPut.put("mobile","");
                if(resCon.getString("primary_email") != null) consPut.put("email", resCon.getString("primary_email"));
                   else  consPut.put("email","");
                consultant.add(consPut);
            }
            resCon.close();
        }
        catch(SQLException sql){
            sql.printStackTrace();
        }

        //construct the agency
        JSONArray agencyArray = new JSONArray();
        String agencyQuery = "select org_name,location,street,address,telephone,email,riremarks,website,markup1 "
        + "from orgs where org_id = '"+org_id+"' and pcc ='"+pcc+"'    limit 1";
        try{
            ResultSet aRes = db.readQuery(agencyQuery);
            while(aRes.next()){
                JSONObject aPut = new JSONObject();
                markup1 =  Integer.parseInt(aRes.getString("markup1"));
                session.setAttribute("markup1",markup1);
                System.out.println("set markup " + markup1);
                aPut.put("org_name", aRes.getString("org_name"));
                aPut.put("location", aRes.getString("location"));
                aPut.put("street", aRes.getString("street"));
                aPut.put("address", aRes.getString("address"));
                aPut.put("telephone", aRes.getString("telephone"));
                aPut.put("email", aRes.getString("email"));
                aPut.put("website", aRes.getString("website"));
                aPut.put("markup1", aRes.getString("markup1"));
                session.setAttribute("riRemarks",aRes.getString("riremarks"));
                agencyArray.add(aPut);
            }
            aRes.close();
        }
        catch(SQLException sql){
            sql.printStackTrace();
        }
        db.close();



        JSONArray namedets  = new JSONArray();
        for(int nameLoop  = 0; nameLoop < firstNarray.size();nameLoop++){
            JSONObject jfname = (JSONObject) firstNarray.get(nameLoop);
            JSONObject jlname = (JSONObject) lastNarray.get(nameLoop);
            JSONObject ptname  = new JSONObject();
            ptname.put("FName",jfname.get("FName"));
            ptname.put("AbsNameNum",jfname.get("AbsNameNum"));
            ptname.put("LName",jlname.get("LName"));
            namedets.add(ptname);
        }

        //.println("names " + namedets);
        JSONArray jmealsA = new JSONArray();
        for(int ml = 0; ml < mealplan.size(); ml ++){
            JSONObject ptmealObj = new JSONObject();
            JSONObject gtmeal = (JSONObject) mealplan.get(ml);
            ptmealObj.put("SegNum",gtmeal.get("SegNum"));
            ptmealObj.put("SSRCode",gtmeal.get("SSRCode"));
            String absnamenum = (String) gtmeal.get("AbsNameNum");
            for(int nm = 0; nm < namedets.size(); nm++){
                JSONObject gtnm = (JSONObject) namedets.get(nm);
                String absnn  = (String) gtnm.get("AbsNameNum");
                if(absnamenum.equals(absnn)){
                    String fnameLname = gtnm.get("FName") + " " +gtnm.get("LName");
                    ptmealObj.put("fnameLname",fnameLname);
                }
            }
            jmealsA.add(ptmealObj);
        }
        jobj.put("namedets",namedets);
        jobj.put("segdata",airSegments);
        jobj.put("ticketnumbers",ticketnumber);
        jobj.put("success",1);
        //get all arrays into a session object
        session.setAttribute("mealplan",jmealsA);
        session.setAttribute("segdata",airSegments);
        session.setAttribute("agencyArray",agencyArray);
        session.setAttribute("consultant", consultant);
        session.setAttribute("weatherdata",weather_array);
        session.setAttribute("ticketnumbers", ticketnumber);

        out.print(jobj);
        }
        else{
            rsp.setContentType("application/json");
            jobj.put("success", 0);
            jobj.put("message", "PNR Sent Was Empty");
            out.print(jobj);
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse rsp)    throws ServletException, IOException {

    }


    public static String ConvertDate(String milliSeconds){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(milliSeconds));
        return simpleDateFormat.format(calendar.getTime());
    }

    private String formatDate(String s){
        String d = s.substring(6,8) + "/" + s.substring(4,6) + "/" + s.substring(0,4);
        return d;
    }


    private String getCityName(String code){
        String d = "";
        String dbconfig = "java:/comp/env/jdbc/database";
        BDB db = new BDB(dbconfig); //log.info("Connection Opened--------------");
        try{
            String sql = "SELECT city_name FROM city_codes WHERE city_code = '" + code.trim() + "' LIMIT 1";
            ResultSet rs = db.readQuery(sql);

            if(rs.next()){
                d = (rs.getString(1));
            }else{
                d = code;
            }
            db.close();

        }catch(SQLException se){
            se.printStackTrace();
        }catch(NullPointerException e){
            e.printStackTrace();
        }
        if(db != null){
            db.close();
            //log.info("Closing Connection --------------");
        }
        return d;
    }

    private String getAirportName(String airname){
        String dbconfig = "java:/comp/env/jdbc/database";
        BDB db = new BDB(dbconfig);
        String airName = "";
        try{
            String aquery = "select cityname from airportcodes  where airportcode =  '"+airname+"' limit 1";
            ResultSet resair = db.readQuery(aquery);
            while(resair.next()){
                airName  = resair.getString("cityname");
            }
            resair.close();
            db.close();

        }
        catch(SQLException sqle){
            sqle.printStackTrace();
        }
        return airName;
    }


    public void  getWeather_xml(String city){
        String inputLine="";
        String xml = null;

    }

}
