import java.io.IOException;
import java.io.PrintWriter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

//import java.text.ParseException;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.annotation.WebServlet;


import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;
import org.json.simple.parser.JSONParser;


import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List;
import java.util.Arrays;

import org.baraza.xml.BXML;
import org.baraza.xml.BElement;
import org.baraza.DB.BDB;
import org.baraza.DB.BQuery;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.ContentHandler;
import org.json.simple.parser.JSONParser;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import java.util.GregorianCalendar;

import java.text.DateFormatSymbols;


@WebServlet("/processpnrsegs2")
public class ProcessSummary extends HttpServlet{
	static Logger log = Logger.getLogger(ProcessPnr.class.getName());
	public static final String KEY_NO_MATCH = "NO_MATCH_FOUND";
	public static String dateFormat = "dd-MM-yyyy hh:mm";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
    @SuppressWarnings("unchecked")
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse rsp)	throws ServletException, IOException {
    HttpSession session = req.getSession(false);
	//printing options
	String printSeparate = req.getParameter("printSeparate");
	//System.out.println("printing options " + printSeparate);
	//options print  dontprint
	//new code for processpnrsegs using C#
    JSONObject allobj = new JSONObject();
    JSONArray allarr = new JSONArray();

	JSONParser jsonParser=new JSONParser();
	JSONObject allData = new JSONObject();
	String bookingData  = (String) session.getAttribute("bookingFile");
	System.out.println("data from C#  " + bookingData);
	//allData = (JSONObject) bookingData;
	JSONParser parser = new JSONParser();
	try{
		allData = (JSONObject) parser.parse(bookingData);
	}
	catch(ParseException pxe){
		pxe.printStackTrace();
	}
	////System.out.println(allData);
	JSONArray fareArrayC = (JSONArray)allData.get("fares");
	//System.out.println("fares from c " + fareArrayC);
	JSONArray nameArrayC = (JSONArray)allData.get("names");
	JSONArray genticketArrayC = (JSONArray)allData.get("passet");
	JSONArray eticketArrayC = (JSONArray)allData.get("etickets");
	JSONArray segArrayC = (JSONArray)allData.get("segments");
	////System.out.println("segment data from CSharp  " + segArrayC +" length: " + segArrayC.size());

	JSONObject itindata = new JSONObject();  //array that contains all report data

	JSONArray fArray,segdata,namedets,jremarkArray,weather_array,mealplan,consultant,agencyArray,seatsArray,ticketnumbers= new JSONArray();
	seatsArray = (JSONArray)session.getAttribute("seatsArray");
    fArray = (JSONArray)allData.get("fares");
	segdata = (JSONArray)session.getAttribute("segdata");
	namedets = (JSONArray)session.getAttribute("namedets");
	jremarkArray = (JSONArray)session.getAttribute("genremarks");
	weather_array = (JSONArray)session.getAttribute("weatherdata");
	mealplan = (JSONArray)session.getAttribute("mealplan");
	consultant = (JSONArray) session.getAttribute("consultant");
	agencyArray = (JSONArray) session.getAttribute("agencyArray");
	ticketnumbers = (JSONArray) session.getAttribute("ticketnumbers");
	//include/exclude fare in the report
	String fares = "fareTrue";
    JSONObject finalfare = new JSONObject();
	if(fares.equals("fareTrue")){
		log.info("process fare");
		//processing the fare
		JSONArray allfareArray = new JSONArray();
		for(int farelen = 0; farelen < fArray.size(); farelen++){
			JSONObject getAllfare = (JSONObject) fArray.get(farelen);
			JSONObject putAllFareObj = new JSONObject();
			putAllFareObj.put("TicketingAgencyPcc",getAllfare.get("TicketingAgencyPcc"));
			putAllFareObj.put("FQNum",getAllfare.get("FQNum"));
			//add the segments for the fare
			/*
			JSONArray fareseg = new JSONArray();
			JSONArray getfareseg = (JSONArray) getAllfare.get("segs");
			for(int fareseglen = 0; fareseglen < getfareseg.size(); fareseglen++){
				JSONObject paxfareobj = (JSONObject) getfareseg.get(fareseglen);
				JSONObject putseg = new JSONObject();
				String segn  = paxfareobj.get("SegNum").toString();
				putseg.put("SegNum",segn);
				putseg.put("EndDate",paxfareobj.get("EndDate"));
				putseg.put("stDate",paxfareobj.get("stDate"));
				//add segment data from segdata
                // "EndAirp": "Nairobi",        "SegNum": "1",       "StartAirp": "Addis Ababa",
				for(int segdatalen = 0; segdatalen < segdata.size(); segdatalen ++){
					JSONObject getsegdata = (JSONObject) segdata.get(segdatalen);
					String segdatasegnum = (String) getsegdata.get("SegNum");
					if(segdatasegnum.equals(segn)){
						putseg.put("starts",getsegdata.get("StartAirp"));
						putseg.put("ends",getsegdata.get("EndAirp"));
						putseg.put("StartTm",getsegdata.get("StartTm"));
						putseg.put("EndTm",getsegdata.get("EndTm"));
						putseg.put("fromTo",getsegdata.get("fromTo"));
					}
				}
                fareseg.add(putseg);
			}
			putAllFareObj.put("segs",fareseg);
			*/




			//add the filed fare

			JSONArray filedfare = (JSONArray) getAllfare.get("fare");
			JSONArray finalfiledfare = new JSONArray();
			for(int filedfarelen = 0; filedfarelen< filedfare.size(); filedfarelen ++){
				JSONObject jgetfiledfare = (JSONObject) filedfare.get(filedfarelen);

                String farepaxType = "";
				JSONObject putfiledfareobj = new JSONObject();
				putfiledfareobj.put("TAmt",jgetfiledfare.get("TAmt"));
				putfiledfareobj.put("baseAmt",jgetfiledfare.get("baseAmt"));
				putfiledfareobj.put("FareConstruction",jgetfiledfare.get("FareConstruction"));
				putfiledfareobj.put("curr",jgetfiledfare.get("curr"));
				putfiledfareobj.put("StoredQuote",jgetfiledfare.get("StoredQuote"));
				putfiledfareobj.put("filDate",jgetfiledfare.get("filDate"));

				/*JSONArray baggage = (JSONArray) jgetfiledfare.get("baggage");
				JSONObject getbaggInfo = (JSONObject) baggage.get(0);
				String bagStr = (String)getbaggInfo.get("baggage");
				putfiledfareobj.put("baggage",bagStr);*/
				//add the passengers
				JSONArray farepax = new JSONArray();
				JSONArray faregetpax = (JSONArray) jgetfiledfare.get("pax");
				for(int farepaxlen = 0; farepaxlen < faregetpax.size(); farepaxlen ++){
					JSONObject getfarepax = (JSONObject) faregetpax.get(farepaxlen);
					JSONObject jputfarepax = new JSONObject();
					jputfarepax.put("name",getfarepax.get("name"));
					jputfarepax.put("paxtype",getfarepax.get("paxtype"));
					farepaxType  = (String)getfarepax.get("paxtype");
					//System.out.println("applies to " + getfarepax.get("name") + " pax type " +  getfarepax.get("paxtype"));
					farepax.add(jputfarepax);
				}
				putfiledfareobj.put("farepaxtype",farepaxType);
				//System.out.println("farepaxtype " + farepaxType);
				putfiledfareobj.put("pax",farepax);


				//put the segment
				JSONArray faretax = new JSONArray();
				JSONArray fareTax= (JSONArray) jgetfiledfare.get("taxes");

				if(fareTax.size() > 0){
					for(int faretaxlen = 0; faretaxlen < fareTax.size(); faretaxlen ++){
						JSONObject getfaretax = (JSONObject) fareTax.get(faretaxlen);
						JSONObject jputfaretax = new JSONObject();
						jputfaretax.put("amount",getfaretax.get("amount"));
						jputfaretax.put("total",getfaretax.get("total"));
						jputfaretax.put("code",getfaretax.get("code"));
						jputfaretax.put("curr",getfaretax.get("curr"));
						faretax.add(jputfaretax);
					}
					putfiledfareobj.put("taxes",faretax);
				}
				else{
					//for(int faretaxlen = 0; faretaxlen < fareTax.size(); faretaxlen ++){
						//JSONObject getfaretax = (JSONObject) fareTax.get(faretaxlen);
						JSONObject jputfaretax = new JSONObject();
						jputfaretax.put("amount","0");
						jputfaretax.put("total",jgetfiledfare.get("baseAmt"));
						jputfaretax.put("code","");
						jputfaretax.put("curr",jgetfiledfare.get("curr"));
						faretax.add(jputfaretax);
					//}
					putfiledfareobj.put("taxes",faretax);
				}
			//System.out.println("farepaxtype  " + farepaxType);
			    //System.out.println("faredataincpax  " + putfiledfareobj);
              finalfiledfare.add(putfiledfareobj);
		  }
		  putAllFareObj.put("fare",finalfiledfare);
          allfareArray.add(putAllFareObj);




		}
		finalfare.put("fares", allfareArray);
	}




	String pax_selected = req.getParameter("pax");
	JSONArray passPutArr = new JSONArray();
	String rFirstName = "TK.";

	if(pax_selected.length() >1){
		String[] pax_array =  pax_selected.replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("\\s", "").split(",");
	    for(int paxno = 0; paxno<pax_array.length;paxno++){
			int paxnumber = Integer.parseInt(pax_array[paxno]);
			JSONObject passPutObj = new JSONObject();
			JSONObject getNameObjC  = (JSONObject) genticketArrayC.get(paxnumber-1);
			passPutObj.put("paxtype", getNameObjC.get("paxtype"));
			passPutObj.put("name",getNameObjC.get("name"));
			passPutObj.put("eticket",getNameObjC.get("eticket"));
			System.out.println("......." + getNameObjC.get("name"));
			passPutObj.put("issue_date",getNameObjC.get("issue_date"));
			passPutObj.put("locator",getNameObjC.get("locator"));
			rFirstName += getNameObjC.get("name").toString();

			//JSONArray jfyer = (JSONArray) getNameObjC.get("flyer");
			//println("array .. " + jfyer);
			//if(jfyer.size() >0){
				//JSONObject getflier = (JSONObject) jfyer.get(0);
				//passPutObj.put("flyer",getflier.get("Number"));
			//}
			//else{
				//passPutObj.put("flyer"," ");
			//}
			passPutArr.add(passPutObj);
		}
		session.setAttribute("reportname",rFirstName.replaceAll(" ","").trim());
		JSONObject test = (JSONObject) passPutArr.get(0);
		System.out.println("+++++++" + test.get("paxtype"));
	}

	//start of segments
	JSONArray segPutArr = new JSONArray();
	JSONArray ssrcodes = new JSONArray();
    //System.out.println(segdata.size() +" and "+ segArrayC.size());
    for(int segnumber = 0; segnumber<segdata.size();segnumber++){
		JSONObject segPutObj = new JSONObject();
		JSONObject getSegObj = (JSONObject) segdata.get(segnumber);
		JSONObject getSegObjC = (JSONObject) segArrayC.get(segnumber);
		String destn = (String) getSegObjC.get("Destination");
		//println("destination   " + destn);
		String stopNon = "Non-Stop";
		String connect = (String) getSegObjC.get("Connection");
		//println("connect: " +connect);
		if(connect.equals("True")){
          stopNon = "Stops";
		}
		String fnum1 =  (String)getSegObjC.get("Carrier");
		String fnum2  = (String)getSegObjC.get("FlightNumber");
		String fnumber  = fnum1.substring(0,2) + " " + fnum2;

		String flighthead  = ((String)getSegObjC.get("fheader")).substring(9,32).replaceAll("   "," ");
		//segPutObj.put("Status",getSegObjC.get("SegmentStatusCode"));
		//returnStatus
		String StatusCode = (String) getSegObjC.get("SegmentStatusCode");
		segPutObj.put("Status",returnStatus(StatusCode));



		segPutObj.put("airName",fnumber);
		segPutObj.put("EndAirp",getSegObj.get("EndAirp"));
		segPutObj.put("StartA",getSegObj.get("StartA"));
		segPutObj.put("EndA",getSegObj.get("EndA"));
		segPutObj.put("stops",stopNon);
		segPutObj.put("operatedby",getSegObjC.get("OperatingFlightName"));
		segPutObj.put("FltNum",getSegObj.get("FltNum"));
		segPutObj.put("arv_terminal",getSegObjC.get("DestinationTerminal"));
		segPutObj.put("EndTm", getSegObj.get("EndTm"));
		segPutObj.put("flightHeader",flighthead);
		segPutObj.put("SegNum2",getSegObj.get("SegNum"));
		String dtstring = (String)getSegObj.get("Dt");
		String frmDate = dtstring.substring(4,6)+"/"+dtstring.substring(6,8)+"/"+dtstring.substring(0,4);
		segPutObj.put("Dt",format_date(frmDate));
		segPutObj.put("duration",getSegObjC.get("Duration"));
		segPutObj.put("farebasis",getSegObjC.get("farebasis"));
		segPutObj.put("airline_name",getSegObjC.get("Carrier"));
		segPutObj.put("dep_terminal",getSegObjC.get("OriginTerminal"));
		segPutObj.put("AirV",getSegObj.get("AirV"));
		segPutObj.put("StartAirp",getSegObj.get("StartAirp"));
		segPutObj.put("StartTm",getSegObj.get("StartTm"));
		segPutObj.put("Class",getSegObj.get("Class"));
		segPutObj.put("cabin",getSegObj.get("cabin"));
		segPutObj.put("eqp",getSegObjC.get("EquipmentType"));
		segPutObj.put("VendorLocator",getSegObjC.get("VendorLocator"));
		String rec = (String) getSegObjC.get("VendorLocator");
		System.out.println(rec + "..............");
		String arriv_date =(String) getSegObjC.get("EndDateTime");
		String date_to_format = arriv_date.substring(0,arriv_date.length() - 10);
		segPutObj.put("arriv_date",format_date(date_to_format));
		segPutObj.put("start_airp",getSegObj.get("start_airp"));
		segPutObj.put("stop_airp",getSegObj.get("stop_airp"));
        //putting the legs

        //System.out.println("reached here...>> ");
		//its the legs that had an issue

		JSONArray legs = (JSONArray)getSegObjC.get("legs");
		JSONArray legs_processed = new JSONArray();
		//System.out.println("legs:..............." + legs);

		for(int lg = 0; lg < legs.size(); lg ++){
			JSONObject jptseg = new JSONObject();
			JSONObject jgetleg = (JSONObject) legs.get(lg);
      //      String carbonE = "";
			//String carbonset = (String)jgetleg.get("carbonset");
			//if(carbonset.equals("yes")){
			//	carbonE =  ((String)jgetleg.get("carbon")).substring(10,19);
			//}
			//else{
			//	carbonE =  (String)jgetleg.get("carbon");
			//}
			jptseg.put("Origin",getAirportName(jgetleg.get("Origin").toString()));
			jptseg.put("Destination",getAirportName(jgetleg.get("Destination").toString()));
			jptseg.put("OriginTerminal",jgetleg.get("OriginTerminal"));
			jptseg.put("FlightDuration",format_Time(jgetleg.get("FlightDuration").toString()));
			jptseg.put("equipment",jgetleg.get("equipment"));
			jptseg.put("DepartureDate",jgetleg.get("DepartureDate"));
			jptseg.put("DestinationTerminal",jgetleg.get("DestinationTerminal"));
			//jptseg.put("carbonE",carbonE);
			legs_processed.add(jptseg);
		}
		segPutObj.put("legs",legs_processed);

		JSONArray jpassSeatsSSR = new JSONArray();
		for(int pas = 0; pas < passPutArr.size(); pas++){
			JSONObject jgetSeats = (JSONObject) passPutArr.get(pas);
			JSONObject jputseats = new JSONObject();
			//"flyer": " ",      "issue_date": "1/1/0001 12:00:00 AM",      "name": "MARY DOE",      "locator": " "
			String pxname = (String)jgetSeats.get("name");
		    jputseats.put("name",jgetSeats.get("name"));
			jputseats.put("flyer",jgetSeats.get("flyer"));
			// add eticket data
			//for (int eticpax=0; eticpax < eticketArrayC.size(); eticpax++) {
				//JSONObject geteticket = (JSONObject) eticketArrayC.get(eticpax);
				//equate the segments
				//String segDet = (String)getSegObj.get("fromTo");
				//String stSeg = (String)geteticket.get("seg");
				//String segdet2 = stSeg.substring(stSeg.length()-7,stSeg.length());
				//String paxnms = (String)geteticket.get("fname");
				//equate names
				//String[] px2 = paxnms.split("/");
				//String pax_str = px2[1] + " " + px2[0];
				//if(pax_str.equals(pxname) && segDet.equals(segdet2)){
				//	jputseats.put("eticket",geteticket.get("eticket"));
				//}
			//}
			//add seats data

			//putting the specialservices mealplan
			//System.out.println("mealplan " +mealplan);
			JSONArray meals = new JSONArray();
			for(int mealNum = 0; mealNum < mealplan.size(); mealNum++){
				JSONObject gtMeals = (JSONObject) mealplan.get(mealNum);
				JSONObject addmealsobj = new JSONObject();
				//equate the segments
				String segAppl  = (String) gtMeals.get("SegNum");
				String segNum = (String) getSegObj.get("SegNum");
				//equate the pax  name pxname
				String mealName = (String) gtMeals.get("fnameLname");  //from meals
				if(segAppl.equals(segNum) && mealName.equals(pxname)){
					addmealsobj.put("SegNum",gtMeals.get("SegNum"));
		            addmealsobj.put("SSRCode",gtMeals.get("SSRCode"));
					addmealsobj.put("fnameLname",gtMeals.get("fnameLname"));
					meals.add(addmealsobj);
				}
			}
			jputseats.put("meals",meals);
            jpassSeatsSSR.add(jputseats);
		}
		segPutObj.put("seatsPass",jpassSeatsSSR);

		segPutArr.add(segPutObj);
	}
	itindata.put("agency", agencyArray);
	itindata.put("consultant", consultant);
	itindata.put("segments", segPutArr);
	JSONArray jsonAll = new JSONArray();
	//loop thro passPutArr
	//adding fare to the names array

	JSONObject addnamesfare = new JSONObject();
	JSONArray addNamesTofare = new JSONArray();

	//dealing with fare jsonAll
	JSONArray jfareAllArr  = (JSONArray) finalfare.get("fares");
	//System.out.println("jfareAllArr final "+jfareAllArr);

	ArrayList<String> list=new ArrayList<String>();
	for(int pass =0; pass < passPutArr.size(); pass ++){
		JSONArray arrnames = new JSONArray();
		JSONObject ptnmes = new JSONObject();
		JSONObject gtnames = (JSONObject) passPutArr.get(pass);
		//JSONObject test = (JSONObject) passPutArr.get(0);
		System.out.println(pass);
		//System.out.println("+++++++" + test.get("paxtype"));
		String pass_name = (String)gtnames.get("name");
		ptnmes.put("name",gtnames.get("name"));
		ptnmes.put("paxtype",gtnames.get("paxtype"));
		//ptnmes.put("eticket",gtnames.get("eticket"));
		System.out.println(">>>>>>>>>>>"+gtnames.get("paxtype"));
		String paxPaxtype = (String) gtnames.get("paxtype");
		System.out.println("......."+paxPaxtype);

		JSONArray allFares = new JSONArray();
		for(int lpfare = 0; lpfare < jfareAllArr.size(); lpfare ++){
		    JSONObject gtfareInfo = (JSONObject) jfareAllArr.get(lpfare);
		    //System.out.println("data  " + gtfareInfo.get("TicketingAgencyPcc"));//working well

			JSONObject putAllfares = new JSONObject();
			putAllfares.put("TicketingAgencyPcc",gtfareInfo.get("TicketingAgencyPcc"));
			//System.out.println(gtfareInfo.get("TicketingAgencyPcc") + "  " + gtfareInfo.get("FQNum"));

			JSONArray faredata = (JSONArray) gtfareInfo.get("fare");
			JSONArray innerfare = new JSONArray();
			for(int faredatalen = 0; faredatalen < faredata.size(); faredatalen++){
				JSONObject getfaredata = (JSONObject) faredata.get(faredatalen);
				String farepastype = (String) getfaredata.get("farepaxtype");
				if(paxPaxtype.equals(farepastype)){
					JSONObject putInnerFare = new JSONObject();
					int mark_up = 0;
					float markup=0;
					if(session.getAttribute("markup1") != null){
						mark_up = (Integer) session.getAttribute("markup1");
						markup = (float) mark_up;
						System.out.println(mark_up);
					}
					float total_amount = 0;
					System.out.println(getfaredata.get("TAmt").toString());
					if(getfaredata.get("TAmt") != null){
						total_amount = Float.valueOf(getfaredata.get("TAmt").toString());
					}
					System.out.println("Reached here here");
					Float total_fare_markup = total_amount + mark_up;
					System.out.println("total amt..... " + total_fare_markup);
					putInnerFare.put("total_fare_markup",total_fare_markup);
					putInnerFare.put("TAmt",getfaredata.get("TAmt"));
					putInnerFare.put("farepaxtype",getfaredata.get("farepaxtype"));
					putInnerFare.put("baseAmt",getfaredata.get("baseAmt"));
					//putInnerFare.put("baggage",getfaredata.get("baggage"));
					putInnerFare.put("FareConstruction",getfaredata.get("FareConstruction"));
					putInnerFare.put("TAmt",getfaredata.get("TAmt"));
					putInnerFare.put("curr",getfaredata.get("curr"));
					putInnerFare.put("StoredQuote",getfaredata.get("StoredQuote"));
					putInnerFare.put("filDate",getfaredata.get("filDate"));
					//get now to the inner json array
					JSONArray innerTaxes = new JSONArray();
					JSONArray getInnerTaxes = (JSONArray) getfaredata.get("taxes");
					for(int taxInt = 0; taxInt < getInnerTaxes.size(); taxInt++){
						JSONObject getTax = (JSONObject) getInnerTaxes.get(taxInt);
						JSONObject puttaxdata = new JSONObject();
						puttaxdata.put("amount",getTax.get("amount"));
						puttaxdata.put("total",getTax.get("total"));
						puttaxdata.put("code",getTax.get("code"));
						puttaxdata.put("curr",getTax.get("curr"));
						innerTaxes.add(puttaxdata);
					}
					putInnerFare.put("taxes",innerTaxes);
					innerfare.add(putInnerFare);
					System.out.println("inner fare " + innerfare);
				}
				//can also close innerfare here
			}
			putAllfares.put("fares",innerfare);
			allFares.add(putAllfares);
		}

		//System.out.println("allFares " + allFares);


       //namewithdata
	    ptnmes.put("assocfare",allFares);
			// add eticket data
			//for (int eticpax=0; eticpax < eticketArrayC.size(); eticpax++) {
				//JSONObject geteticket = (JSONObject) eticketArrayC.get(eticpax);
				//equate the segments
				//String segDet = (String)getSegObj.get("fromTo");
				//String stSeg = (String)geteticket.get("seg");
				//String segdet2 = stSeg.substring(stSeg.length()-7,stSeg.length());
				//String paxnms = (String)geteticket.get("fname");
				//equate names
				//String[] px2 = paxnms.split("/");
				//String pax_str = px2[1] + " " + px2[0];
				//if(pax_str.equals(pass_name)){
				//	ptnmes.put("eticket",geteticket.get("eticket"));
				//}
			//}
		ptnmes.put("issue_date",gtnames.get("issue_date"));
		ptnmes.put("locator",gtnames.get("locator"));
		ptnmes.put("flyer",gtnames.get("flyer"));
		arrnames.add(ptnmes);

		itindata.put("namedets",arrnames);
		itindata.put("namewithdata",arrnames);
		list.add(itindata.toString());
		System.out.println("namede " +arrnames);//working perfectly
	}

	String listString = list.toString();
	JSONArray jData = new JSONArray();
	try{
		jData = (JSONArray)parser.parse(listString);
	}
	catch(ParseException pxe){
		pxe.printStackTrace();
	}


	allobj.put("alldata",jData);
	allobj.put("message","Electronic Ticket");
	System.out.println("............... " + allobj);
	allobj.put("jreport",allobj.toString());

	System.out.println("successfully generated report data.... ");


	PrintWriter out = rsp.getWriter();
	out.print(allobj);
	}

	private String getAirportName(String airname){
        String dbconfig = "java:/comp/env/jdbc/database";
        BDB db = new BDB(dbconfig);
        String airName = "";
        try{
            String aquery = "select cityname from airportcodes  where airportcode =  '"+airname+"' limit 1";
            ResultSet resair = db.readQuery(aquery);
            while(resair.next()){
                airName  = resair.getString("cityname");
            }
            resair.close();
            db.close();

        }
        catch(SQLException sqle){
            sqle.printStackTrace();
        }
        return airName;
    }


	private String format_date(String datestr){
		String date_formatted = "";
		try{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			Date theDate = format.parse(datestr);
			Calendar myCal = new GregorianCalendar();
			myCal.setTime(theDate);
			int monthnum = Integer.valueOf(myCal.get(Calendar.MONTH));
			date_formatted  = myCal.get(Calendar.DAY_OF_MONTH) + "th "+ new DateFormatSymbols().getMonths()[monthnum] + "," + myCal.get(Calendar.YEAR);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	    return date_formatted;
	}

	private String format_Time(String s){
		String hours = s.substring(0,2);
		String minutes = s.substring(3,5);
		String re_string = hours + " hrs " + minutes +" mins";
		return re_string;
	}



	private String returnStatus(String statusCode){
		JSONObject statusObj = new JSONObject();
		JSONArray statusArr = new JSONArray();
		statusObj.put("KK","KK-CONFIRMED");
		statusObj.put("HK","HK-CONFIRMED");
		statusObj.put("KL","KL-CONFIRMED");
		statusObj.put("TK","TK-CONFIRMED");
		statusObj.put("RR","RR-CONFIRMED");
		statusObj.put("AK","AK-CONFIRMED");
		statusObj.put("ZK","ZK-CONFIRMED");
		statusObj.put("BK","BK-CONFIRMED");
		statusObj.put("MK","MK-CONFIRMED");
		statusObj.put("HX","HX-CANCELLED");
		statusObj.put("UN","UN-CANCELLED");
		statusObj.put("NO","NO-CANCELLED");
		statusObj.put("UC","UC-CANCELLED");
		statusObj.put("TL","TL-WAITLISTED");
		statusObj.put("HL","HL-WAITLISTED");
		statusObj.put("US","US-WAITLISTED");
		statusObj.put("UU","UU-WAITLISTED");
		statusObj.put("LL","LL-WAITLISTED");
		statusObj.put("AL","AL-WAITLISTED");
		statusObj.put("BL","BL-WAITLISTED");
		statusObj.put("NN","NN-NOT COMPLETE");
		statusObj.put("HS","HS-NOT COMPLETE");
		statusArr.add(statusObj);
		JSONObject statObj = (JSONObject)statusArr.get(0);
		return statObj.get(statusCode).toString();
	}
}
