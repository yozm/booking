#!/bin/bash

cd $(dirname $0)


TOMCAT_PATH=/opt/tomcat7
WEBAPP_NAME=ticketproducer
export PATH=/opt/jdk1.7.0_79/bin/
export CLASSPATH=$TOMCAT_PATH/lib/servlet-api.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/baraza.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/json_simple-1.1.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/java-json.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/itextpdf-5.5.6.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/xmlworker-5.5.6.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/jsoup-1.8.2.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/poi-3.10.jar

export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/commons-httpclient.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/commons-logging.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/joda-time-1.6.jar

export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/json-simple-1.1.1.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/jasperreports-6.1.0.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/ant-1.7.1.jar
export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/lib/commons-io-2.4.jar

export CLASSPATH=$CLASSPATH:$TOMCAT_PATH/webapps/$WEBAPP_NAME/WEB-INF/classes/


echo 'Compiling.......'
javac ProcessPNRXMLToSegments.java
javac Report.java
javac Validating.java
javac ProcessPnr.java
javac ProcessC.java
javac ProcessSummary.java
echo 'Compile Done'
