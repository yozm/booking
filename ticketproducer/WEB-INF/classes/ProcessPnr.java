import java.io.IOException;
import java.io.PrintWriter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

//import java.text.ParseException;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.annotation.WebServlet;


import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;

import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List;
import java.util.Arrays;

import org.baraza.xml.BXML;
import org.baraza.xml.BElement;
import org.baraza.DB.BDB;
import org.baraza.DB.BQuery;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.ContentHandler;
import org.json.simple.parser.JSONParser;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import java.util.GregorianCalendar;

import java.text.DateFormatSymbols;


@WebServlet("/processpnrsegs")
public class ProcessPnr extends HttpServlet{
	static Logger log = Logger.getLogger(ProcessPnr.class.getName());
	public static final String KEY_NO_MATCH = "NO_MATCH_FOUND";
	public static String dateFormat = "dd-MM-yyyy hh:mm";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
    @SuppressWarnings("unchecked")
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse rsp)	throws ServletException, IOException {
    HttpSession session = req.getSession(false);
	//printing options
	String printSeparate = req.getParameter("printSeparate");
	System.out.println("printing options " + printSeparate);
	//options print  dontprint
	//new code for processpnrsegs using C#
	JSONParser jsonParser=new JSONParser();
	JSONObject allData = new JSONObject();
	String bookingData  = (String) session.getAttribute("bookingFile");
	//allData = (JSONObject) bookingData;
	JSONParser parser = new JSONParser();
	try{
		allData = (JSONObject) parser.parse(bookingData);
	}
	catch(ParseException pxe){
		pxe.printStackTrace();
	}
	//System.out.println(allData);
	JSONArray seatArrayC = (JSONArray)allData.get("seats");
	JSONArray eticketArrayC = (JSONArray)allData.get("etickets");
	JSONArray ticketspassengerC = (JSONArray)allData.get("passet");
	JSONArray fareArrayC = (JSONArray)allData.get("fares");
	JSONArray nameArrayC = (JSONArray)allData.get("names");
	JSONArray segArrayC = (JSONArray)allData.get("segments");
	System.out.println("segment data from CSharp  " + segArrayC +" length: " + segArrayC.size());

	JSONObject itindata = new JSONObject();  //array that contains all report data
	//including the RI remarks
	JSONArray rmkArray =new JSONArray();
	String remarks = req.getParameter("remarks");
	if(remarks.equals("remarksTrue")){
		JSONObject putRemarks = new JSONObject();
		putRemarks.put("remarks",session.getAttribute("riRemarks"));
		rmkArray.add(putRemarks);
	}

	if(remarks.equals("remarksFalse")){
		//log.info("no processing");
	}
	JSONArray fArray,segdata,namedets,jremarkArray,weather_array,mealplan,consultant,agencyArray,seatsArray,ticketnumbers= new JSONArray();
	seatsArray = (JSONArray)session.getAttribute("seatsArray");
    fArray = (JSONArray)allData.get("fares");
	segdata = (JSONArray)session.getAttribute("segdata");
	namedets = (JSONArray)session.getAttribute("namedets");
	jremarkArray = (JSONArray)session.getAttribute("genremarks");
	weather_array = (JSONArray)session.getAttribute("weatherdata");
	mealplan = (JSONArray)session.getAttribute("mealplan");
	consultant = (JSONArray) session.getAttribute("consultant");
	agencyArray = (JSONArray) session.getAttribute("agencyArray");
	ticketnumbers  = (JSONArray) session.getAttribute("ticketnumbers");
	//include/exclude fare in the report
	String fares = req.getParameter("fare");

	if(fares.equals("fareTrue")){
		log.info("process fare");
		//processing the fare
		JSONArray allfareArray = new JSONArray();
		for(int farelen = 0; farelen < fArray.size(); farelen++){
			JSONObject getAllfare = (JSONObject) fArray.get(farelen);
			JSONObject putAllFareObj = new JSONObject();
			putAllFareObj.put("TicketingAgencyPcc",getAllfare.get("TicketingAgencyPcc"));
			putAllFareObj.put("FQNum",getAllfare.get("FQNum"));
			//add the segments for the fare
			JSONArray fareseg = new JSONArray();
			JSONArray getfareseg = (JSONArray) getAllfare.get("segs");
			for(int fareseglen = 0; fareseglen < getfareseg.size(); fareseglen++){
				JSONObject paxfareobj = (JSONObject) getfareseg.get(fareseglen);
				JSONObject putseg = new JSONObject();
				String segn  = paxfareobj.get("SegNum").toString();
				putseg.put("SegNum",segn);
				putseg.put("EndDate",paxfareobj.get("EndDate"));
				putseg.put("stDate",paxfareobj.get("stDate"));
				//add segment data from segdata
                // "EndAirp": "Nairobi",        "SegNum": "1",       "StartAirp": "Addis Ababa",
				for(int segdatalen = 0; segdatalen < segdata.size(); segdatalen ++){
					JSONObject getsegdata = (JSONObject) segdata.get(segdatalen);
					String segdatasegnum = (String) getsegdata.get("SegNum");
					if(segdatasegnum.equals(segn)){
						putseg.put("starts",getsegdata.get("StartAirp"));
						putseg.put("ends",getsegdata.get("EndAirp"));
						putseg.put("StartTm",getsegdata.get("StartTm"));
						putseg.put("EndTm",getsegdata.get("EndTm"));
						putseg.put("fromTo",getsegdata.get("fromTo"));
					}
				}
                fareseg.add(putseg);
			}
			putAllFareObj.put("segs",fareseg);




			//add the filed fare
			JSONArray filedfare = (JSONArray) getAllfare.get("fare");
			JSONArray finalfiledfare = new JSONArray();
			for(int filedfarelen = 0; filedfarelen< filedfare.size(); filedfarelen ++){
				JSONObject jgetfiledfare = (JSONObject) filedfare.get(filedfarelen);
				JSONObject putfiledfareobj = new JSONObject();
				putfiledfareobj.put("TAmt",jgetfiledfare.get("TAmt"));
				putfiledfareobj.put("baseAmt",jgetfiledfare.get("baseAmt"));
				putfiledfareobj.put("curr",jgetfiledfare.get("curr"));
				putfiledfareobj.put("StoredQuote",jgetfiledfare.get("StoredQuote"));
				putfiledfareobj.put("filDate",jgetfiledfare.get("filDate"));
				//add the passengers
				JSONArray farepax = new JSONArray();
				JSONArray faregetpax = (JSONArray) jgetfiledfare.get("pax");
				for(int farepaxlen = 0; farepaxlen < faregetpax.size(); farepaxlen ++){
					JSONObject getfarepax = (JSONObject) faregetpax.get(farepaxlen);
					JSONObject jputfarepax = new JSONObject();
					jputfarepax.put("name",getfarepax.get("name"));
					farepax.add(jputfarepax);
				}
				putfiledfareobj.put("pax",farepax);

				//put the segment
				JSONArray faretax = new JSONArray();
				JSONArray fareTax= (JSONArray) jgetfiledfare.get("taxes");

				if(fareTax.size() > 0){
					for(int faretaxlen = 0; faretaxlen < fareTax.size(); faretaxlen ++){
						JSONObject getfaretax = (JSONObject) fareTax.get(faretaxlen);
						JSONObject jputfaretax = new JSONObject();
						jputfaretax.put("amount",getfaretax.get("amount"));
						jputfaretax.put("total",getfaretax.get("total"));
						jputfaretax.put("code",getfaretax.get("code"));
						jputfaretax.put("curr",getfaretax.get("curr"));
						faretax.add(jputfaretax);
					}
					putfiledfareobj.put("taxes",faretax);
				}
				else{
					//for(int faretaxlen = 0; faretaxlen < fareTax.size(); faretaxlen ++){
						//JSONObject getfaretax = (JSONObject) fareTax.get(faretaxlen);
						JSONObject jputfaretax = new JSONObject();
						jputfaretax.put("amount","0");
						jputfaretax.put("total",jgetfiledfare.get("baseAmt"));
						jputfaretax.put("code","");
						jputfaretax.put("curr",jgetfiledfare.get("curr"));
						faretax.add(jputfaretax);
					//}
					putfiledfareobj.put("taxes",faretax);
				}


              finalfiledfare.add(putfiledfareobj);
			}
			putAllFareObj.put("fare",finalfiledfare);
          allfareArray.add(putAllFareObj);
		}
		itindata.put("fares", allfareArray);
	}
	if(fares.equals("fareFalse")){
	    log.info("no fare processing");
	}

	String pax_selected = req.getParameter("pax");
	JSONArray passPutArr = new JSONArray();
	String rFirstName = "TK.";

	if(pax_selected.length() >1){
		String[] pax_array =  pax_selected.replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("\\s", "").split(",");
	    for(int paxno = 0; paxno<pax_array.length;paxno++){
			int paxnumber = Integer.parseInt(pax_array[paxno]);
			JSONObject passPutObj = new JSONObject();
			JSONObject getNameObjC  = (JSONObject)nameArrayC.get(paxnumber-1);
			passPutObj.put("name",getNameObjC.get("name"));
			//passPutObj.put("issue_date",getNameObjC.get("issue_date"));
			String issue_date = (String)session.getAttribute("issue_date");
			passPutObj.put("issue_date",session.getAttribute("issue_date"));
			passPutObj.put("locator",getNameObjC.get("locator"));
			//passPutObj.put("baggage", getNameObjC.get("baggage"));
			String baggage = (String)getNameObjC.get("baggage");
			passPutObj.put("baggage", baggage);
			System.out.println("Baggage "+ baggage);
			System.out.println("Issue_date " + issue_date);
			//String eticket = (String)getNameObjC.get("eticket");
			//if (eticket == null) {
			//	passPutObj.put("eticket", "No Ticket Data");
			//}
			//else {
			//passPutObj.put("eticket", eticket);
		//}
			rFirstName += getNameObjC.get("name").toString();

			JSONArray jfyer = (JSONArray) getNameObjC.get("flyer");
			//println("array .. " + jfyer);
			if(jfyer.size() >0){
				JSONObject getflier = (JSONObject) jfyer.get(0);
				passPutObj.put("flyer",getflier.get("Number"));
			}


			else{
				passPutObj.put("flyer"," ");
			}


				//JSONArray eticarray = (JSONArray) getNameObjC.get("etickets");
				//JSONArray eticarrays = new JSONArray();
				//for (int et=0; et< eticarray.size();et++) {
				//JSONObject geteticket = new JSONObject();
				//	JSONObject jgeteticket = (JSONObject) eticarray.get(et);
				//	geteticket.put("eticket",jgeteticket.get("eticket"));
				//eticarray.put("eticket", geteticket.get("eticket"));
				//eticarrays.add(geteticket);
				//passPutObj.add(eticarray);
			//}
			//passPutObj.put("eticket", eticarrays);



			passPutArr.add(passPutObj);
		}

		//options print  dontprint
		//possibility of printing more than one report
		//System.out.println("to be reportname " +rFirstName.replaceAll(" ","").trim());
		session.setAttribute("reportname",rFirstName.replaceAll(" ","").trim());
	}
	//println("passPutArr " + passPutArr);
	//end of names

	//start of segments
	String seg_selected = req.getParameter("seg");
	JSONArray segPutArr = new JSONArray();
	JSONArray ssrcodes = new JSONArray();
	if(seg_selected.length() >1){
		String[] seg_array =  seg_selected.replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("\\s", "").split(",");

        for(int segno = 0; segno<seg_array.length;segno++){
			int segnumber = Integer.parseInt(seg_array[segno]);
			JSONObject segPutObj = new JSONObject();

			JSONObject getSegObj = (JSONObject) segdata.get(segnumber-1);
			JSONObject getSegObjC = (JSONObject) segArrayC.get(segnumber-1);
			String destn = (String) getSegObjC.get("Destination");
			//println("destination   " + destn);
			String stopNon = "Non-Stop";
			String stopNum = "";
			String connect = (String) getSegObjC.get("Connection");
			System.out.println("connect: " +connect);
			if(connect.equals("True")){
              stopNon = "Stops";
			}
			String stops = (String) getSegObjC.get("NumberOfStops");
			if (stops.equals("0")) {
				stopNum = "Non-Stop";
			}
			else {
				stopNum="Stops";
			}
			String fnum1 =  (String)getSegObjC.get("Carrier");
			String fnum2  = (String)getSegObjC.get("FlightNumber");
			String fnumber  = fnum1.substring(0,2) + " " + fnum2;

			String flighthead  = ((String)getSegObjC.get("fheader")).substring(9,32).replaceAll("   "," ");
			//segPutObj.put("Status",getSegObjC.get("SegmentStatusCode"));
			//returnStatus
			String StatusCode = (String) getSegObjC.get("SegmentStatusCode");
			segPutObj.put("Status",returnStatus(StatusCode));



			segPutObj.put("airName",fnumber);
			segPutObj.put("EndAirp",getSegObj.get("EndAirp"));
			segPutObj.put("StartA",getSegObj.get("StartA"));
			segPutObj.put("EndA",getSegObj.get("EndA"));
			segPutObj.put("stops",stopNum);
			segPutObj.put("operatedby",getSegObjC.get("OperatingFlightName"));
			segPutObj.put("FltNum",getSegObj.get("FltNum"));
			segPutObj.put("arv_terminal",getSegObjC.get("DestinationTerminal"));
			segPutObj.put("EndTm", getSegObj.get("EndTm"));
			segPutObj.put("flightHeader",flighthead);
			segPutObj.put("SegNum2",getSegObj.get("SegNum"));
			String dtstring = (String)getSegObj.get("Dt");
			String frmDate = dtstring.substring(4,6)+"/"+dtstring.substring(6,8)+"/"+dtstring.substring(0,4);
			segPutObj.put("Dt",format_date(frmDate));
			segPutObj.put("duration",getSegObjC.get("Duration"));
			segPutObj.put("airline_name",getSegObjC.get("Carrier"));
			segPutObj.put("dep_terminal",getSegObjC.get("OriginTerminal"));
			segPutObj.put("AirV",getSegObj.get("AirV"));
			segPutObj.put("StartAirp",getSegObj.get("StartAirp"));
			segPutObj.put("StartTm",getSegObj.get("StartTm"));
			segPutObj.put("Class",getSegObj.get("Class"));
			segPutObj.put("cabin",getSegObj.get("cabin"));
			segPutObj.put("eqp",getSegObjC.get("EquipmentType"));
			segPutObj.put("VendorLocator",getSegObjC.get("VendorLocator"));
			String rec = (String) getSegObjC.get("VendorLocator");
			System.out.println(rec + "..............");
			String arriv_date =(String) getSegObjC.get("EndDateTime");
			String date_to_format = arriv_date.substring(0,arriv_date.length() - 10);
			segPutObj.put("arriv_date",format_date(date_to_format));
			segPutObj.put("start_airp",getSegObj.get("start_airp"));
			segPutObj.put("stop_airp",getSegObj.get("stop_airp"));
            //putting the legs

            System.out.println("reached here...>> ");
			//its the legs that had an issue

			JSONArray legs = (JSONArray)getSegObjC.get("legs");
			JSONArray legs_processed = new JSONArray();
			System.out.println("legs:..............." + legs);

			for(int lg = 0; lg < legs.size(); lg ++){
				JSONObject jptseg = new JSONObject();
				JSONObject jgetleg = (JSONObject) legs.get(lg);
                String carbonE = "";
				String carbonset = (String)jgetleg.get("carbonset");
				System.out.println("carbonset " + carbonset);
				if(carbonset.equals("yes")){
					carbonE =  ((String)jgetleg.get("carbon")).substring(10,19);
				}
				else{
					carbonE =  (String)jgetleg.get("carbon");
				}
				jptseg.put("Origin",getAirportName(jgetleg.get("Origin").toString()));
				jptseg.put("Destination",getAirportName(jgetleg.get("Destination").toString()));
				jptseg.put("OriginTerminal",jgetleg.get("OriginTerminal"));
				jptseg.put("FlightDuration",format_Time(jgetleg.get("FlightDuration").toString()));
				jptseg.put("equipment",jgetleg.get("equipment"));
				jptseg.put("DepartureDate",jgetleg.get("DepartureDate"));
				jptseg.put("DestinationTerminal",jgetleg.get("DestinationTerminal"));
				jptseg.put("carbonE",carbonE);
				legs_processed.add(jptseg);
			}
			segPutObj.put("legs",legs_processed);

			JSONArray jpassSeatsSSR = new JSONArray();
			for(int pas = 0; pas < passPutArr.size(); pas++){
				JSONObject jgetSeats = (JSONObject) passPutArr.get(pas);
				JSONObject jputseats = new JSONObject();
				//"flyer": " ",      "issue_date": "1/1/0001 12:00:00 AM",      "name": "MARY DOE",      "locator": " "
				String pxname = (String)jgetSeats.get("name");
			    jputseats.put("name",jgetSeats.get("name"));
				jputseats.put("flyer",jgetSeats.get("flyer"));
				jputseats.put("baggage", jgetSeats.get("baggage"));
				//jputseats.put("eticket", jgetSeats.get("eticket"));
				jputseats.put("carbonE", jgetSeats.get("carbonE"));
				//add eticket data
				for (int eticpax=0; eticpax < eticketArrayC.size(); eticpax++) {
					JSONObject geteticket = (JSONObject) eticketArrayC.get(eticpax);
					//equate the segments
					String segDet = (String)getSegObj.get("fromTo");
					String stSeg = (String)geteticket.get("seg");
					String segdet2 = stSeg.substring(stSeg.length()-7,stSeg.length());
					String paxnms = (String)geteticket.get("fname");
					//equate names
					String[] px2 = paxnms.split("/");
					String pax_str = px2[1] + " " + px2[0];
					if(pax_str.equals(pxname) && segDet.equals(segdet2)){
						jputseats.put("eticket",geteticket.get("eticket"));
					}
				}
				//add seats data
				for(int seatpax = 0; seatpax < seatArrayC.size(); seatpax++){
					JSONObject getseat = (JSONObject) seatArrayC.get(seatpax);
					//equate the segments
					String segDet = (String)getSegObj.get("fromTo");
					String stSeg = (String)getseat.get("seg");
					String segdet2 = stSeg.substring(stSeg.length()-7,stSeg.length());
					String paxnms = (String)getseat.get("pax");
					//equate names
					String[] px2 = paxnms.split("/");
					String pax_str = px2[1] + " " + px2[0];
					if(pax_str.equals(pxname) && segDet.equals(segdet2)){
						jputseats.put("seat",getseat.get("seat"));
					}
				}

				//putting the specialservices mealplan
				//System.out.println("mealplan " +mealplan);
				JSONArray meals = new JSONArray();
				for(int mealNum = 0; mealNum < mealplan.size(); mealNum++){
					JSONObject gtMeals = (JSONObject) mealplan.get(mealNum);
					JSONObject addmealsobj = new JSONObject();
					//equate the segments
					String segAppl  = (String) gtMeals.get("SegNum");
					String segNum = (String) getSegObj.get("SegNum");
					//equate the pax  name pxname
					String mealName = (String) gtMeals.get("fnameLname");  //from meals
					if(segAppl.equals(segNum) && mealName.equals(pxname)){
						addmealsobj.put("SegNum",gtMeals.get("SegNum"));
			            addmealsobj.put("SSRCode",gtMeals.get("SSRCode"));
								addmealsobj.put("fnameLname",gtMeals.get("fnameLname"));
						meals.add(addmealsobj);
					}
				}
				jputseats.put("meals",meals);
                jpassSeatsSSR.add(jputseats);
			}
			segPutObj.put("seatsPass",jpassSeatsSSR);

			segPutArr.add(segPutObj);
		}
	}
    itindata.put("tickremarks", rmkArray);
	itindata.put("agency", agencyArray);
	itindata.put("consultant", consultant);
	itindata.put("segments", segPutArr);
	itindata.put("namedets", passPutArr);
	itindata.put("genremarks",jremarkArray);
	itindata.put("weatherdata",weather_array);
	itindata.put("jreport",itindata.toString());
	System.out.println("successfully generated report data.... ");




	PrintWriter out = rsp.getWriter();
	out.print(itindata);
	}

	private String getAirportName(String airname){
        String dbconfig = "java:/comp/env/jdbc/database";
        BDB db = new BDB(dbconfig);
        String airName = "";
        try{
            String aquery = "select cityname from airportcodes  where airportcode =  '"+airname+"' limit 1";
            ResultSet resair = db.readQuery(aquery);
            while(resair.next()){
                airName  = resair.getString("cityname");
            }
            resair.close();
            db.close();

        }
        catch(SQLException sqle){
            sqle.printStackTrace();
        }
        return airName;
    }


	private String format_date(String datestr){
		String date_formatted = "";
		try{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			Date theDate = format.parse(datestr);
			Calendar myCal = new GregorianCalendar();
			myCal.setTime(theDate);
			int monthnum = Integer.valueOf(myCal.get(Calendar.MONTH));
			date_formatted  = myCal.get(Calendar.DAY_OF_MONTH) + "th "+ new DateFormatSymbols().getMonths()[monthnum] + "," + myCal.get(Calendar.YEAR);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	    return date_formatted;
	}

	private String format_Time(String s){
		String hours = s.substring(0,2);
		String minutes = s.substring(3,5);
		String re_string = hours + " hrs " + minutes +" mins";
		return re_string;
	}



	private String returnStatus(String statusCode){
		JSONObject statusObj = new JSONObject();
		JSONArray statusArr = new JSONArray();
		statusObj.put("KK","KK-CONFIRMED");
		statusObj.put("HK","HK-CONFIRMED");
		statusObj.put("KL","KL-CONFIRMED");
		statusObj.put("TK","TK-CONFIRMED");
		statusObj.put("RR","RR-CONFIRMED");
		statusObj.put("AK","AK-CONFIRMED");
		statusObj.put("ZK","ZK-CONFIRMED");
		statusObj.put("BK","BK-CONFIRMED");
		statusObj.put("MK","MK-CONFIRMED");
		statusObj.put("HX","HX-CANCELLED");
		statusObj.put("UN","UN-CANCELLED");
		statusObj.put("NO","NO-CANCELLED");
		statusObj.put("UC","UC-CANCELLED");
		statusObj.put("TL","TL-WAITLISTED");
		statusObj.put("HL","HL-WAITLISTED");
		statusObj.put("US","US-WAITLISTED");
		statusObj.put("UU","UU-WAITLISTED");
		statusObj.put("LL","LL-WAITLISTED");
		statusObj.put("AL","AL-WAITLISTED");
		statusObj.put("BL","BL-WAITLISTED");
		statusObj.put("NN","NN-NOT COMPLETE");
		statusObj.put("HS","HS-NOT COMPLETE");
		statusArr.add(statusObj);
		JSONObject statObj = (JSONObject)statusArr.get(0);
		return statObj.get(statusCode).toString();
	}




}
