import java.io.IOException;
import java.io.PrintWriter;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;

import org.baraza.xml.BXML;
import org.baraza.xml.BElement;
import org.baraza.DB.BDB;
import org.baraza.DB.BQuery;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


@WebServlet("/validating")
public class Validating extends HttpServlet{
	static Logger log = Logger.getLogger(Validating.class.getName());
	RequestDispatcher dispatcher;


	@SuppressWarnings("unchecked")
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	throws ServletException, IOException {
	      // System.out.println("getting data");

	}//doget

	@SuppressWarnings("unchecked")
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(false);
		String tag = req.getParameter("tag");
		if(tag.equals("") || tag == null){
			dispatcher = req.getRequestDispatcher("/");
			dispatcher.forward(req, resp);
		}
       if(tag.equals("authenticate")){
		//log.info("Reached Push To processrequest auth : tag " + tag);
		String dbconfig = "java:/comp/env/jdbc/database";
		BDB db = new BDB(dbconfig);//log.info("Connection Opened--------------");
		int success = 0; String message = "You Are Not Subscribed to e-Ticket Producer.";
		String son = req.getParameter("son");
		String pcc = req.getParameter("pcc");
		String app = req.getParameter("app");

		if(son != null && son != "" && pcc != null && pcc != ""){

			session.setAttribute("pcc",null);
		  session.setAttribute("son",null);
		  session.setAttribute("name", null);
		  session.setAttribute("org", null);
		  session.setAttribute("org_id", null);
		  session.setAttribute("entity_id", null);

			String user_query = "SELECT entity_id, org_id, son, entity_name,app_name, phone_ph, phone_pa, phone_pb, phone_pt, pcc, org_name, gds_free_field, show_fare, logo, table_id, "
		                          + " table_name, premises, street, post_office_box, town, sys_country_name,  postal_code, phone_number, email "
		                          + " FROM vw_app_users WHERE app_name='"+app+"' AND son = '" + son + "' AND pcc = '" + pcc+ "' LIMIT 1";
		      log.info("SQL : " + user_query);
			ResultSet res = db.readQuery(user_query);
			success = 0; message = "You Are Not Subscribed to this service.";
			try{
				while(res.next()){
					System.out.println("app_name"+res.getString("app_name"));
					session.setAttribute("pcc",pcc);
			    session.setAttribute("son",son);
			    session.setAttribute("name",res.getString("entity_name"));
			    session.setAttribute("org",res.getString("org_name"));
			    session.setAttribute("org_id", res.getString("org_id"));
			    session.setAttribute("entity_id", res.getString("entity_id"));
          success = 1; message = "Authentication Successfull. Redirecting to TicketProducer......";
				}
			}
			catch(SQLException sqle){
				log.info("there was an sql error!");
			}
			}
			else{
				success = 0; message = "Permission Denied : Invalid PCC and SON ";
			}

			if(db != null){
				db.close();
				//log.info("Closing Connection --------------");
			}
            log.info(message);
			resp.setContentType("application/json");
			PrintWriter out = resp.getWriter();
			JSONObject jobj = new JSONObject();
			jobj.put("success", success);
			jobj.put("message", message);
			out.print(jobj);
			//log.info("JSON  VALIDATE USER : " + jobj.toString());
		}

	}//dopost
}
