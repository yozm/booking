
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%response.addHeader("Cache-Control","no-cache");%>
<%response.addHeader("Expires","-1");%>
<%response.addHeader("Pragma","no-cache");%>
<%
//System.out.println(session.getAttribute("bookingFile"));
%>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>E-Ticket Producer</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/> -->
<link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/TicketProducer/css/style.css" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" href="assets/TicketProducer/img/favicon.png"/>

</head>

<body onload="getUserDetails()">
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <img class="" alt="Brand" src="assets/TicketProducer/img/travelport.png">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse text-center">
            <h3 class="navbar-text navbar-right" style="margin-right:15px;">TicketProducer</h3>

        </div><!-- /.navbar-collapse -->
    </div>
</div><!--/navbar-->
<div class="container-fluid">
    <div class="row" style="margin-top:1px">
        <div class="col-md-12 text-center">
            <h3>TicketProducer</h3>
        </div><!--/col-->
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="assets/TicketProducer/img/spinner.GIF" class="hidden" id="loading_gif" />
        </div><!--/col-->
    </div><!--/row-->
    <div class="row" style="margin-top:20px">
        <div class="col-md-12 text-center">
            <p class="" style="padding:15px;" id="lblResponse">Please Wait...</p>
        </div><!--/col-->
    </div>
    <div class="row" style="margin-top:20px">
        <div class="col-md-12 text-center">
            <form>
                <div class="form-group">
                    <button id="btnRetry" type="button"  class="btn btn-success hidden" onclick="getUserDetails()"> <span class="glyphicon   glyphicon-repeat" aria-hidden="true"></span> Retry  </button>&nbsp;
                </div>

                 <input type="hidden" id="tx_son" name="tx_son" />
                 <input type="hidden" id="tx_pcc" name="tx_pcc" />
                 <input type="hidden" id="tx_app" name="tx_app" value="TicketProducer" />
             </form>
        </div><!--/col-->
    </div><!--/row-->
    <div id="response" style="color:#009DDC"></div>
</div><!--/container-fluid-->

<div class="footer">
  <div class="container">

    <p class="text-muted">2014 &copy; Travelport Kenya Agency ${appName}. | Developed by <a href="http://www.dewcis.com">Dew CIS Solutions</a></p>
  </div>
</div><!--/footer-->

<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/TicketProducer/js/index.js" type="text/javascript"></script>
<script src="assets/TicketProducer/js/common.js" ></script>
</body>
</html>
