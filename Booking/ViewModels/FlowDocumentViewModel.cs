﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FlowDocumentViewModel.cs" company="Name of the company">
//   Copyright (c).  All rights reserved.
// </copyright>
// <summary>
//   ViewModel for creating simple flow document
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Booking.ViewModels
{
    using System.Collections.ObjectModel;
    using Travelport.MvvmHelper.ViewModels;

    /// <summary>
    /// ViewModel for creating simple flow document
    /// </summary>
    public class FlowDocumentViewModel : ViewModelBase
    {
        #region private variables

        /// <summary>
        /// Backing field for IsHeaderLineVisible
        /// </summary>
        private bool isHeaderLineVisible;

        /// <summary>
        /// Backing field for HeaderLine
        /// </summary>
        private string headerLine;

        /// <summary>
        /// Backing field for InfoMessages collection
        /// </summary>
        private ObservableCollection<string> infoMessages;

        /// <summary>
        /// Backing field for FlowDocumentItems collection
        /// </summary>
        private ObservableCollection<FlowDocumentItemViewModel> flowDocumentItems;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FlowDocumentViewModel"/> class.
        /// </summary>
        public FlowDocumentViewModel()
        {
            this.Initialize();
        }

        #endregion

        #region public properties

        /// <summary>
        /// Gets or sets a value indicating whether HeaderLine is visible.
        /// </summary>
        public bool IsHeaderLineVisible
        {
            get
            {
                return this.isHeaderLineVisible;
            }

            set
            {
                this.isHeaderLineVisible = value;
                this.OnPropertyChanged("IsHeaderLineVisible");
            }
        }

        /// <summary>
        /// Gets or sets the HeaderLine.
        /// </summary>
        public string HeaderLine
        {
            get
            {
                return this.headerLine;
            }

            set
            {
                this.headerLine = value;
                this.OnPropertyChanged("HeaderLine");
            }
        }

        /// <summary>
        /// Gets or sets the InfoMessages.
        /// </summary>
        public ObservableCollection<string> InfoMessages
        {
            get
            {
                return this.infoMessages;
            }

            set
            {
                this.infoMessages = value;
                this.OnPropertyChanged("InfoMessages");
            }
        }

        /// <summary>
        /// Gets or sets the FlowDocumentItems.
        /// </summary>
        public ObservableCollection<FlowDocumentItemViewModel> FlowDocumentItems
        {
            get
            {
                return this.flowDocumentItems;
            }

            set
            {
                this.flowDocumentItems = value;
                this.OnPropertyChanged("FlowDocumentItems");
            }
        }

        #endregion

        #region private methods

        /// <summary>
        /// Initializes the properties.
        /// </summary>
        private void Initialize()
        {
            this.IsHeaderLineVisible = true;
            this.HeaderLine = "This is a header line";
            this.InfoMessages = new ObservableCollection<string> { "Info Message", "Info Message1", "Info Message2" };
            this.FlowDocumentItems = new ObservableCollection<FlowDocumentItemViewModel>
                                    {
                                        new FlowDocumentItemViewModel
                                            {
                                                LineNumber = "1",
                                                Origin = "ATL",
                                                Destination = "BOS",
                                                StartTime = "0000",
                                                EndTime = "0100",
                                                CarrierCode = "BA",
                                                FlightNumber = "202",
                                                AvailableClassOfService = "J0 C0 D0 I0 Z0 W9 S9 H0 K0 Y9",
                                            },
                                        new FlowDocumentItemViewModel
                                            {
                                                LineNumber = "2",
                                                Origin = "ATL",
                                                Destination = "BOS",
                                                StartTime = "1200",
                                                EndTime = "1500",
                                                CarrierCode = "DL",
                                                FlightNumber = "232",
                                                AvailableClassOfService = "J0 C0 D0 I0 Z0 W9 S9 H0 K0 Y9",
                                            },
                                        new FlowDocumentItemViewModel
                                            {
                                                LineNumber = "3",
                                                Origin = "ATL",
                                                Destination = "BOS",
                                                StartTime = "1100",
                                                EndTime = "1400",
                                                CarrierCode = "BA",
                                                FlightNumber = "321",
                                                AvailableClassOfService = "J0 C0 D0 I0 Z0 W9 S9 H0 K0 Y9",
                                            }
                                    };
        }

        #endregion
    }
}