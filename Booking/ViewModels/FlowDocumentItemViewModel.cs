﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FlowDocumentItemViewModel.cs" company="Name of the company">
//   Copyright (c).  All rights reserved.
// </copyright>
// <summary>
//   Represents an individual flow document line as a Span to be added
//   to an FlowDocumentView Paragraph
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Booking.ViewModels
{
    using Travelport.MvvmHelper.ViewModels;
    using Travelport.Smartpoint.Common;
    using Views;

    /// <summary>
    /// Represents an individual flow document line as a Span to be added
    /// to an FlowDocumentView Paragraph
    /// </summary>
    [View(ViewType = typeof(FlowDocumentItemView))]
    public class FlowDocumentItemViewModel : ViewModelBase
    {
        #region private variables

        /// <summary>
        /// Backing field for LineNumber
        /// </summary>
        private string lineNumber;

        /// <summary>
        /// Backing field for CarrierCode
        /// </summary>
        private string carrierCode;

        /// <summary>
        /// Backing field for Origin
        /// </summary>
        private string origin;

        /// <summary>
        /// Backing field for Destination
        /// </summary>
        private string destination;

        /// <summary>
        /// Backing field for StartTime
        /// </summary>
        private string startTime;

        /// <summary>
        /// Backing field for EndTime
        /// </summary>
        private string endTime;

        /// <summary>
        /// Backing field for FlightNumber
        /// </summary>
        private string flightNumber;

        /// <summary>
        /// Backing field for AvailableClassOfService
        /// </summary>
        private string availableClassOfService;

        #endregion

        #region public properties

        /// <summary>
        /// Gets or sets the Line number.
        /// </summary>
        public string LineNumber
        {
            get
            {
                return this.lineNumber;
            }

            set
            {
                this.lineNumber = value;
                this.OnPropertyChanged("LineNumber");
            }
        }

        /// <summary>
        /// Gets or sets the Carrier code.
        /// </summary>
        public string CarrierCode
        {
            get
            {
                return this.carrierCode;
            }

            set
            {
                this.carrierCode = value;
                this.OnPropertyChanged("CarrierCode");
            }
        }

        /// <summary>
        /// Gets or sets the Origin.
        /// </summary>
        public string Origin
        {
            get
            {
                return this.origin;
            }

            set
            {
                this.origin = value;
                this.OnPropertyChanged("Origin");
            }
        }

        /// <summary>
        /// Gets or sets the Destination.
        /// </summary>
        public string Destination
        {
            get
            {
                return this.destination;
            }

            set
            {
                this.destination = value;
                this.OnPropertyChanged("Destination");
            }
        }

        /// <summary>
        /// Gets or sets the Start time.
        /// </summary>
        public string StartTime
        {
            get
            {
                return this.startTime;
            }

            set
            {
                this.startTime = value;
                this.OnPropertyChanged("StartTime");
            }
        }

        /// <summary>
        /// Gets or sets the End time.
        /// </summary>
        public string EndTime
        {
            get
            {
                return this.endTime;
            }

            set
            {
                this.endTime = value;
                this.OnPropertyChanged("EndTime");
            }
        }

        /// <summary>
        /// Gets or sets the Flight number.
        /// </summary>
        public string FlightNumber
        {
            get
            {
                return this.flightNumber;
            }

            set
            {
                this.flightNumber = value;
                this.OnPropertyChanged("FlightNumber");
            }
        }

        /// <summary>
        /// Gets or sets the available class of service.
        /// </summary>
        public string AvailableClassOfService
        {
            get
            {
                return this.availableClassOfService;
            }

            set
            {
                this.availableClassOfService = value;
                this.OnPropertyChanged("AvailableClassOfService");
            }
        }

        #endregion
    }
}
