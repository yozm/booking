﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RetrievePNRViewModel.cs" company="Name of the company">
//   Copyright (c).  All rights reserved.
// </copyright>
// <summary>
//   ViewModel for the view derives from ViewModelBase class available in MvvmHelper assembly.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Booking.ViewModels
{
    using Common;
    using Travelport.MvvmHelper;
    using Travelport.MvvmHelper.ViewModels;

    /// <summary>
    /// ViewModel for the view derives from ViewModelBase class available in MVVMHelper assembly.
    /// </summary>
    public class RetrievePNRViewModel : ViewModelBase
    {
        #region private variables

        /// <summary>
        /// Pnr locator code.
        /// </summary>
        private string locator;

        /// <summary>
        /// The retrieveCommand.
        /// </summary>
        private RelayCommand retrieveCommand;

        #endregion

        #region public properties

        /// <summary>
        /// Gets or sets the PNR Locator code. This property is bound to the textbox 
        /// </summary>
        public string Locator
        {
            get
            {
                return this.locator;
            }

            set
            {
                this.locator = value;
                this.OnPropertyChanged("Locator");
            }
        }

        /// <summary>
        /// Gets the Command for retrieving the booking file
        /// </summary>
        public RelayCommand RetrieveCommand
        {
            get
            {
                return this.retrieveCommand ?? (this.retrieveCommand = new RelayCommand(this.ExecuteRetrieveCommand, o => !string.IsNullOrEmpty(Locator)));
            }
        }

        #endregion

        #region command handlers

        /// <summary>
        /// Handler for Retrieve Command. 
        /// </summary>
        /// <param name="parameter">Command Parameter</param>
        private void ExecuteRetrieveCommand(object parameter)
        {
            // Create and send the *Locator command
            Helper.SendTerminalCommand(string.Format("*{0}", this.Locator));
        }

        #endregion
    }
}
