﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Handler.cs" company="Name of the company">
//   Copyright (c).  All rights reserved.
// </copyright>
// <summary>
//   Handles the terminal input and output
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Booking.Common
{
    using System;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Documents;
    using Travelport.Smartpoint.Common;
    using Travelport.Smartpoint.Helpers.UI;
    using Travelport.TravelData;
    using ViewModels;
    using Views;

    /// <summary>
    /// Handles the terminal input and output
    /// </summary>
    internal class Handler
    {
        #region Class private data
        /// <summary>
        /// Command to trigger our application
        /// </summary>
        private const string Trigger = "#CS";

        /// <summary>
        /// The Smartpoint window container
        /// </summary>
        private static ISmartDialogWindow spWindow;

        /// <summary>
        /// Our template window content
        /// </summary>
        private static RetrievePNRView csWindow;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Handler"/> class.
        /// </summary>
        public Handler()
        {
            this.Initialize();
        }

        #endregion

        #region Input handler
        /// <summary>
        /// Have an input command, check if something interesting
        /// #CS POPUP - Displays a Popup
        /// #CS WINDOW - Displays a window for retrieving PNR
        /// #CS BF - displays current booking file.
        /// #CS Con - Displays the current connection
        /// #CS MSG - Displays a message box
        /// #CS FLOW - Display a flow document created using MVVM on terminal window
        /// </summary>
        /// <param name="e">The terminal event arguments</param>
        public void HandleInputCommand(TerminalEventArgs e)
        {
            // Check if the command starts with our trigger string
            Regex regex = new Regex(@"^\#CS\s{0,1}(?<command>.*)$", RegexOptions.IgnoreCase);
            Match match = regex.Match(e.TerminalEntry);
            if (match.Success)
            {
                string command = match.Groups["command"].Value;

                // Parse the command
                switch (command.ToUpper())
                {
                    case "?":
                        Helper.SendTerminalOutput(Environment.NewLine + "CS: I only know ?, BF, CON, MSG, Flow, Popup and Window");
                        break;
                    case "POPUP":
                        Helper.ShowPopup(string.Format("CS: The Window is {0}", Environment.GetEnvironmentVariable("csWindow")), 5);
                        Helper.SendTerminalOutput(string.Empty);
                        break;
                    case "WINDOW":
                        this.StartWindow();
                        Helper.SendTerminalOutput(string.Empty);
                        break;
                    case "BF":
                        BookingFile bf = Helper.GetBookingFile();
                        if (bf == null || bf.RecordLocator == null)
                        {
                            Helper.SendTerminalOutput(Environment.NewLine + "CS: No booking file open.");
                        }
                        else
                        {
                            Helper.SendTerminalOutput(Environment.NewLine + string.Format("CS: Booking file locator: {0}", bf.RecordLocator));
                        }

                        break;
                    case "CON":
                        Helper.SendTerminalOutput(Environment.NewLine + string.Format("CS: CON={0}", Helper.GetDesktopFactory().ConnectionName));
                        break;
                    case "MSG":
                        UIHelper.Instance.ShowMessageBox("CS: This is a sample Message Dialog.", MessageBoxImage.Information);
                        Helper.SendTerminalOutput(string.Empty);
                        break;
                    case "FLOW":
                        UIHelper.Instance.SetTerminalContent(UIHelper.Instance.CurrentTEControl, this.MakeFlowDocument(), true);
                        e.TerminalEntry = string.Empty;
                        break;
                    default:
                        Helper.SendTerminalOutput(Environment.NewLine + "CS: I only know ?, BF, CON, MSG, FLOW, POPUP and WINDOW");
                        break;
                }

                // Mark that we handled the command
                e.Handled = true;
            }
        }
        #endregion

        #region Output handler
        /// <summary>
        /// Have an output command, check if we want to filter the output
        /// </summary>
        /// <param name="e">The terminal event arguments</param>
        internal void HandleOutputCommand(TerminalEventArgs e)
        {
        }
        #endregion

        #region Create template window
        /// <summary>
        /// Create or Activate our template window
        /// </summary>
        private void StartWindow()
        {
            if (spWindow == null)
            {
                // Create the Smartpoint window, and make the contents our template control
                spWindow = UIHelper.Instance.CreateNewSmartDialogWindow("SIMPLE_TEMPLATE_VIEW");
                csWindow = new RetrievePNRView { DataContext = new RetrievePNRViewModel() };
                spWindow.Content = csWindow;

                // Set the initial size and caption
                spWindow.Width = csWindow.Width;
                spWindow.Height = csWindow.Height;
                spWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                spWindow.Title = Properties.Resources.WINDOW_TITLE;

                // Hook the closed event so we can cleanup
                spWindow.Closed += this.SpWindow_Closed;

                // Set the environment variable that we are open
                Environment.SetEnvironmentVariable("csWindow", "Open");

                // All set, display the window
                spWindow.Show();
            }
            else
            {
                // Already showing, activate the window
                spWindow.Activate();
            }
        }

        /// <summary>
        /// Event handler when the Smart point window is closed
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event parameters</param>
        private void SpWindow_Closed(object sender, EventArgs e)
        {
            // Remove our event handler
            spWindow.Closed -= this.SpWindow_Closed;

            // Clear our variables
            csWindow = null;
            spWindow = null;

            // Set the environment variable that we are closed
            Environment.SetEnvironmentVariable("csWindow", "Closed");
        }
        #endregion

        #region Create flow document
        /// <summary>
        /// Create a flow document
        /// </summary>
        /// <returns>Returns a populated flow document</returns>
        private Paragraph MakeFlowDocument()
        {
            var flowDocumentView = new FlowDocumentView { DataContext = new FlowDocumentViewModel() };

            // Return the document
            return flowDocumentView;
        }

        #endregion

        #region Initialization
        /// <summary>
        /// Perform any needed initialization
        /// </summary>
        private void Initialize()
        {
            // Set the environment variable that we never opened
            Environment.SetEnvironmentVariable("csWindow", "Never Opened");
        }
        #endregion

    }
}
