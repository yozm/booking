﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Plugin.cs" company="Name of the company">
//  Copyright (c). All rights reserved.
// </copyright>
// <summary>
//   Plugin for Travelport smartpoint
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Booking
{
    using Common;
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using Travelport.Smartpoint.Common;
    using Travelport.Smartpoint.Controls;
    using Travelport.Smartpoint.Helpers.Core;
    using Travelport.Smartpoint.Helpers.PNR;
    using Travelport.Smartpoint.Helpers.UI;
    using Travelport.TravelData;
    using System.Linq;
    using System.Collections.Generic;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using Travelport.Smartpoint;
    using Travelport.Smartpoint.Controls.Pnr;
    using Booking.Controls;
    using Travelport.Smartpoint.Controls.Pnr.ViewModels;
    using System.Web;
    using System.Text;
    using Travelport.TravelData.Domain.Common;
    using SimpleJSON;
    using System.Net;
    using System.IO;
    using System.Windows.Input;
    using Travelport.TravelData.Factory;
    using Travelport.TravelData.Domain.Air;
    using Travelport.TravelData.Response;





    /// <summary>
    /// Plugin for extending Smartpoint application
    /// </summary>
    [SmartPointPlugin(Developer = "Name of the developer",
                      Company = "Name of the Company",
                      Description = "Small description about the plugin",
                      Version = "Version number of the plugin",
                      Build = "Build version of Smartpoint application")]
    public class Plugin : HostPlugin
    {
        //BookingFile bf = (BookingFile)PnrHelper.Instance.RetrieveCurrentBookingFile(UIHelper.Instance.CurrentTEControl.Connection);
        /// <summary>
        /// Handles the terminal input and output
        /// </summary>
        private Handler handler;
        /// <summary>
        /// Executes the load actions for the plugin.
        /// </summary>
        public override  void Execute()
        {
            // Attach a handler to execute when the Smartpoint application is ready and all windows are loaded.
            CoreHelper.Instance.OnSmartpointReady += this.OnSmartpointReady;
            this.handler = new Handler();
            // Shows a message in the terminal control
        }

        #region Handlers
        /// <summary>
        /// Handles the Smartpoint Ready event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Travelport.Smartpoint.Helpers.Core.CoreHelperEventArgs"/> instance containing the event data.</param>
        private void OnSmartpointReady(object sender, CoreHelperEventArgs e)
        {   
            //load the ui when smartpoint starts
            // Hook into any terminal commands we are interested in
            // Commands entered by the user before they are executed
            CoreHelper.Instance.TerminalCommunication.OnTerminalPreSend += this.OnTerminalPreSend;
            // Output before it is displayed
            CoreHelper.Instance.TerminalCommunication.OnTerminalPreResponse += this.OnTerminalPreResponse;
            CoreHelper.Instance.TerminalCommunication.OnTerminalPreSend += this.OnTerminalPreSendHandler;
            //UIHelper.Instance.ShowMessageInTerminal("loading a plugin...", UIHelper.Instance.CurrentTEControl);
            PnrViewerControl pnrViewer = UIHelper.Instance.CurrentPnrViewerControl;
            //this.setPopUp(pnrViewer.BookingFile, pnrViewer);
            this.addPnrCustomButton();
        }


        private void setPopUp(BookingFile bf, PnrViewerControl pnrViewer)
        {   
            var window = UIHelper.Instance.CreateNewSmartBrowserWindow("BrowserWindow1");
            window.Content = new UIControl();
            window.Width = 800;
            window.Height = 500;
            window.Show();
        }

        private void addPnrCustomButton() {
            //initialize uicontrol
            var grid = new Grid { Background = new SolidColorBrush(Colors.AliceBlue) };
            grid.RowDefinitions.Clear();
            grid.ColumnDefinitions.Clear();
            grid.Width = 2000;
            grid.Height = 2000;
            grid.ToolTip = "Corporate Script";

            Ellipse ellipse1 = new Ellipse { Width = 20, Height = 20, StrokeThickness = 2, Stroke = new SolidColorBrush(Colors.Black), Fill = new SolidColorBrush(Colors.Gray), };
            UIControl control = new UIControl();
            SmartPnrCustomToolbarButton toolbarButton = new SmartPnrCustomToolbarButton();
            toolbarButton.ButtonContentDefault = ellipse1;
            toolbarButton.Style = (Style)toolbarButton.FindResource("PnrCustomToolbarPopup");
            toolbarButton.View = new UIControl();
            var teControl = UIHelper.Instance.CurrentSmartTerminalWindow;
            teControl.PnrCustomToolbar.Add(toolbarButton);
        }

        

        /// <summary>
        /// Traps plugin specific cryptic entries and process them.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">Arguments from the terminal.</param>
        private void OnTerminalPreSend(object sender, TerminalEventArgs e)
        {
            this.handler.HandleInputCommand(e);
        }

        /// <summary>
        /// Have terminal output
        /// </summary>
        /// <param name="sender">The sending object</param>
        /// <param name="e">The terminal event arguments</param>
        private void OnTerminalPreResponse(object sender, TerminalEventArgs e)
        {
            this.handler.HandleOutputCommand(e);
        }







        public string getBookingFile()
        {

            BookingFile bf = (BookingFile)PnrHelper.Instance.RetrieveCurrentBookingFile(UIHelper.Instance.CurrentTEControl.Connection);
            //i have placed the code here
            // Update segment/leg details
            var factory = UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory as GalileoDesktopFactory;

            //var FiledFareBag = new JSONArray();

            foreach (var item in bf.AirSegments)
            {
                AirSegment airSegment = item;
                factory.SetSegmentDetails(airSegment);
            }

            IEnumerator<AirSegment> segObjData = bf.AirSegments.GetEnumerator();
            var jobj = new JSONObject();
            if (bf.AirSegments.Count > 0)
            {
                //get the ticket issue date 
                var issuedate = factory.RetrieveCurrentBookingFile();
                BookingFileTicketIssue TicketIssue = issuedate.TicketIssue;
                var issue_date = TicketIssue.ActionDateTime;
                MessageBox.Show("issue date " + issue_date.ToString());



                
                //record locator
                var locator = bf.RecordLocator;
                var rloc = bf.Rloc;


                //dealing with ssrcodes
                ReadOnlyCollection<BookingFileManualSsr> ManualSsrs = bf.ManualSsrs;
                ReadOnlyCollection<BookingFileProgrammaticSsr> ProgrammaticSsrs = bf.ProgrammaticSsrs;
                //dealing with seats
                var seats = new JSONArray();
                ReadOnlyCollection<SeatAllocation> SeatAllocations = bf.SeatAllocations;
                foreach (SeatAllocation value in SeatAllocations)
                {
                    var ptSeat = new JSONObject();
                    ptSeat.Add("pax", value.Passenger.ToString());
                    ptSeat.Add("seat", value.RowNumber.ToString() + value.SeatCode.ToString());
                    ptSeat.Add("seg", value.Segment.ToString());
                    ptSeat.Add("StatusCode", value.StatusCode.ToString());
                    //will do some additions t determing the segment number
                    seats.Add(ptSeat);
                }
                jobj.Add("seats", seats);

                //construct names 
                var names = new JSONArray();
                ReadOnlyCollection<Person> passNames = bf.Passengers;
                //var lobjFACTbag = UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory;
                int counter = 0;

                var et = new JSONArray();
                var et_factory = UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory as GalileoDesktopFactory;
                ReadOnlyCollection<string> gt_et = et_factory.SendTerminalCommand("*HTE");
          

                foreach (Person pass in passNames)
                {
                    counter++;
                    MessageBox.Show(counter.ToString());
                    var ptdata = new JSONObject();
                    ptdata.Add("name", pass.FullName.ToString());
                    ptdata.Add("fname", pass.LastName + "/" + pass.FirstName);
                    ptdata.Add("issue_date", issue_date.ToString());
                    ptdata.Add("locator", (locator == null) ? " " : locator);
                    ptdata.Add("paxtype", (pass.PassengerType == null) ? "Adult" : pass.PassengerType.ToString());
                    
                    //add frequent flyer number 
                    Collection<FrequentFlyer> FrequentFlyerCards = pass.FrequentFlyerCards;
                    var flyArray = new JSONArray();
                    foreach (FrequentFlyer freq in FrequentFlyerCards)
                    {
                        var pt_flyer = new JSONObject();
                        pt_flyer.Add("ActionCode ", (freq.ActionCode == null) ? " " : freq.ActionCode.ToString());
                        pt_flyer.Add("Airline", (freq.Airline == null) ? " " : freq.Airline.ToString());
                        pt_flyer.Add("Number", (freq.Number == null) ? " " : freq.Number.ToString());
                        pt_flyer.Add("Airline", (freq.Airline == null) ? " " : freq.Airline.ToString());
                        flyArray.Add(pt_flyer);
                    }
                  
                    var objPNRbag = factory.RetrieveCurrentBookingFile();
                    IEnumerator<AirFiledFareQuote> lobjAFFQbag = objPNRbag.FiledFares.GetEnumerator();
                    Collection<int> filedFaresNumBag = new Collection<int>();
                    while (lobjAFFQbag.MoveNext())
                    {
                        filedFaresNumBag.Add(lobjAFFQbag.Current.Number);
                    }
                    objPNRbag = factory.RetrieveCurrentBookingFile(filedFaresNumBag);
                    var fareArraybag = new JSONArray();
                    string baggageallowed = "";
                    string name = "";

                    foreach (string eti in gt_et.Take(counter + 2))
                    {
                        var etl = eti.Length;
                        var ets = etl - 17;
                        if (etl > 40)
                        {
                            MessageBox.Show("eti" + eti.Substring(ets, 17));
                            ptdata.Add("eticket",eti.Substring(ets, 17));
                        }
                    };


                        foreach (AirFiledFareQuote airfarebag in objPNRbag.FiledFares.Take(1))
                        {
                            MessageBox.Show(airfarebag.TicketingAgencyPcc);
                            ReadOnlyCollection<Person> personbag = objPNRbag.Passengers;

                            var filedFarebag = new JSONArray();
                            ReadOnlyCollection<AirFiledFare> Faresbag = airfarebag.Fares;
                            //var eticket = new JSONObject();
                            var eticket = airfarebag.TicketingInformation;
                            //if (name.Equals(pass.FullName.ToString()))
                            //{

                            if (pass.PassengerType.ToString() == "Adult")
                            {

                                foreach (AirFiledFare faresbag in Faresbag.Take(1))
                                {

                                    var bagAllowance = new JSONArray();
                                    ReadOnlyCollection<AirFiledFareSegment> faresegInfobag = faresbag.SegmentInformation;
                                    foreach (AirFiledFareSegment seginfo in faresegInfobag.Take(counter))
                                    {


                                        MessageBox.Show(name + pass.FullName.ToString());

                                        var ptbaggage = new JSONObject();
                                        baggageallowed = seginfo.BaggageAllowance;
                                        MessageBox.Show(baggageallowed);
                                        ptdata.Add("baggage", baggageallowed);
                                    }
                                    //ptbaggage.Add("baggage", baggageallowed);
                                    //bagAllowance.Add(ptbaggage);
                                    //legputobj.Add(ptbaggage);
                                    //ptdata.Add("baggage", baggageallowed);
                                }

                                MessageBox.Show(baggageallowed);
                                //}
                            }


                                //filedFarebag.Add(bagAllowance);
                            //fareArraybag.Add(filedFarebag);

                        }
                    
                           

                    ptdata.Add("flyer", flyArray);
                

                    names.Add(ptdata);
                }
                jobj.Add("names", names);



                //including carbon emmission
                var carbobArray = new JSONArray();
                var carbon_factory = UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory as GalileoDesktopFactory;
                ReadOnlyCollection<string> gt_carbon = carbon_factory.SendTerminalCommand("*SVC");
                factory.SendTerminalCommand("MD");
                factory.SendTerminalCommand("MD");
            

                /*var lobjFACTbag = UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory;
                var objPNRbag = lobjFACTbag.RetrieveCurrentBookingFile();
                IEnumerator<AirFiledFareQuote> lobjAFFQbag = objPNRbag.FiledFares.GetEnumerator();
                Collection<int> filedFaresNumBag = new Collection<int>();



                var FiledFareBag = new JSONArray();

                while (lobjAFFQbag.MoveNext())
                {
                    filedFaresNumBag.Add(lobjAFFQbag.Current.Number);
                }
                objPNRbag = lobjFACTbag.RetrieveCurrentBookingFile(filedFaresNumBag);
                var fareArraybag = new JSONArray();
                string baggageallowed = "";*/

                //construct the segments
                var jsegs = new JSONArray();
                ReadOnlyCollection<AirSegment> airsegment = bf.AirSegments;
                var count = 0;
                foreach (var airseg in airsegment)
                {
                    //gt_carbon = carbon_factory.SendTerminalCommand("MD");
                    count++;
                    var jsegPt = new JSONObject();
                    jsegPt.Add("number", airseg.SegmentNumber);
                    jsegPt.Add("AirpChange", airseg.AirportChange);
                    jsegPt.Add("Carrier", airseg.Carrier.ToString());
                    jsegPt.Add("ClassOfService", airseg.ClassOfService.ToString());
                    jsegPt.Add("Connection", airseg.Connection.ToString());
                    jsegPt.Add("Destination", airseg.Destination.ToString());
                    jsegPt.Add("DestinationTerminal", (airseg.DestinationTerminal == null) ? " " : airseg.DestinationTerminal);
                    jsegPt.Add("Duration", (airseg.Duration == null) ? " " : airseg.Duration.ToString());
                    jsegPt.Add("DurationTotalHours", (airseg.DurationTotalHours == null) ? " " : airseg.DurationTotalHours.ToString());
                    jsegPt.Add("EndDateTime", (airseg.EndDateTime == null) ? " " : airseg.EndDateTime.ToString());
                    jsegPt.Add("EquipmentType", (airseg.EquipmentType == null) ? " " : airseg.EquipmentType.ToString());
                    jsegPt.Add("FlightDetailsRefKey", (airseg.FlightDetailsRefKey == null) ? " " : airseg.FlightDetailsRefKey);
                    jsegPt.Add("FlightHaveBeenFlown", airseg.FlightHaveBeenFlown);
                    jsegPt.Add("FlightNumber", (airseg.FlightNumber == null) ? " " : airseg.FlightNumber.ToString());
                    jsegPt.Add("HostReservationReference ", (airseg.HostReservationReference == null) ? " " : airseg.HostReservationReference.ToString());
                    jsegPt.Add("NumberOfStops", (airseg.NumberOfStops == null) ? " " : airseg.NumberOfStops.ToString());
                    jsegPt.Add("OperatingFlightName ", (airseg.OperatingFlightName == null) ? " " : airseg.OperatingFlightName.ToString());
                    jsegPt.Add("OriginTerminal", (airseg.OriginTerminal == null) ? " " : airseg.OriginTerminal.ToString());
                    jsegPt.Add("SegmentStatusCode", (airseg.SegmentStatusCode == null) ? " " : airseg.SegmentStatusCode.ToString());
                    jsegPt.Add("StopoverIgnoreInd", (airseg.StopoverIgnoreInd == null) ? " " : airseg.StopoverIgnoreInd.ToString());
                    jsegPt.Add("OperatingFlightName", (airseg.OperatingFlightName == null) ? " " : airseg.OperatingFlightName.ToString());
                    jsegPt.Add("OperatingFlightName", (airseg.OperatingFlightName == null) ? " " : airseg.OperatingFlightName.ToString());
                    jsegPt.Add("BookingClass", (airseg.BookingClass == null) ? " " : airseg.BookingClass.ToString());
                    jsegPt.Add("FlightStatus", (airseg.FlightStatus == null) ? " " : airseg.FlightStatus.ToString());
                    jsegPt.Add("LastSeatAvailabilityPossible", (airseg.LastSeatAvailabilityPossible == null) ? " " : airseg.LastSeatAvailabilityPossible.ToString());
                    jsegPt.Add("FlightStatus ", (airseg.FlightStatus == null) ? " " : airseg.FlightStatus.ToString());
                    jsegPt.Add("OperatedBy", (airseg.OperatedBy == null) ? " " : airseg.OperatedBy.ToString());
                    jsegPt.Add("Origin", (airseg.Origin == null) ? " " : airseg.Origin.ToString());
                    jsegPt.Add("OriginTerminal", (airseg.OriginTerminal == null) ? " " : airseg.OriginTerminal.ToString());
                    var objPNRbasis = factory.RetrieveCurrentBookingFile();
                    IEnumerator<AirFiledFareQuote> lobjAFFQbasis = objPNRbasis.FiledFares.GetEnumerator();
                    Collection<int> filedFaresNumBasis = new Collection<int>();
                    while (lobjAFFQbasis.MoveNext())
                    {
                        filedFaresNumBasis.Add(lobjAFFQbasis.Current.Number);
                    }
                    objPNRbasis = factory.RetrieveCurrentBookingFile(filedFaresNumBasis);
                    var fareArraybasis = new JSONArray();
                    foreach (AirFiledFareQuote airfarebasis in objPNRbasis.FiledFares.Take(1))
                    {

                        var filedFarebag = new JSONArray();
                        ReadOnlyCollection<AirFiledFare> Faresbasis = airfarebasis.Fares;

                            foreach (AirFiledFare faresbasis in Faresbasis.Take(1))
                            {

                                var bagAllowance = new JSONArray();
                                ReadOnlyCollection<AirFiledFareSegment> faresegInfobasis = faresbasis.SegmentInformation;
                                foreach (AirFiledFareSegment seginfo in faresegInfobasis.Take(count))
                                {

                                    var ptfarebasis = new JSONObject();
                                    string farebasis = seginfo.FareBasis;
                                    MessageBox.Show(farebasis);
                                    jsegPt.Add("farebasis", farebasis);
                                }
                            }
                    }

                    //putting the airleg
                    var legArray = new JSONArray();
                    foreach (var leg in airseg.Legs)
                    {
                        var legputobj = new JSONObject();
                        legputobj.Add("DepartureDate", ((leg.DepartureDate == null) ? " " : leg.DepartureDate.ToString()));
                        legputobj.Add("Destination", ((leg.Destination == null) ? " " : leg.Destination.ToString()));
                        legputobj.Add("DestinationTerminal", ((leg.DestinationTerminal == null) ? " " : leg.DestinationTerminal.ToString()));
                        legputobj.Add("FlightDuration", ((leg.FlightDuration == null) ? " " : leg.FlightDuration.ToString()));
                        legputobj.Add("Origin", ((leg.Origin == null) ? " " : leg.Origin.ToString()));
                        legputobj.Add("OriginTerminal", ((leg.OriginTerminal == null) ? " " : leg.OriginTerminal.ToString()));

                        //add the carbon emmission

                        var origin_dest = leg.Origin.ToString() + leg.Destination.ToString();//getting  ADDNBO
                        //MessageBox.Show(origin_dest);
                        /*foreach (AirFiledFareQuote airfarebag in objPNRbag.FiledFares)
                        {
                            var filedFarebag = new JSONArray();
                            ReadOnlyCollection<AirFiledFare> Faresbag = airfarebag.Fares;
                            foreach (AirFiledFare faresbag in Faresbag)
                            {
                                var bagAllowance = new JSONArray();
                                ReadOnlyCollection<AirFiledFareSegment> faresegInfobag = faresbag.SegmentInformation;
                                foreach (AirFiledFareSegment seginfo in faresegInfobag)
                                
                                {
                                    var ptbaggage = new JSONObject();
                                    baggageallowed = seginfo.BaggageAllowance;
                                    //ptbaggage.Add("baggage", baggageallowed);
                                    //bagAllowance.Add(ptbaggage);
                                    //legputobj.Add(ptbaggage);
                                    legputobj.Add("baggage", baggageallowed);

                                    MessageBox.Show(baggageallowed);
                                }
                                //filedFarebag.Add(bagAllowance);
                            }
                            //fareArraybag.Add(filedFarebag);
                        }
                        //legputobj.Add(fareArraybag);
                        //MessageBox.Show(fareArraybag);*/

                        
                        var carbonset = "no";
                        gt_carbon = carbon_factory.SendTerminalCommand("MD");
                        foreach (string co2 in gt_carbon)
                        {
                            var ll = co2.Length;
                            if (ll > 20)
                            {
                                var or_dest = co2.Substring(7, 8).ToString();
                                var compareStr = (origin_dest.ToString().Trim().Equals(or_dest.ToString().Trim()));
                                //MessageBox.Show("comparing " + origin_dest.ToString().Trim() + "  " + or_dest.ToString().Trim() + " " + compareStr);
                                if (compareStr == true)
                                {
                                    legputobj.Add("carbon", co2.Substring(17, 19) + "kgs");
                                    carbonset = "yes";
                                    //MessageBox.Show("carbon emmission...." + co2.Substring(17, 19));
                                }
                            }
                        }


                        if (carbonset == "no")
                        {
                            legputobj.Add("carbon", "kgs");
                        }
                        legputobj.Add("carbonset", carbonset);


                        /*var vendorCode = new JSONArray();
                        var vendor_locator = UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory as GalileoDesktopFactory;
                        ReadOnlyCollection<string> gt_vendor = vendor_locator.SendTerminalCommand("*VL");
                        
                        foreach (string vl in gt_vendor)
                        {
                            string vll = vl.Substring(5, 7);
                            legputobj.Add("VendorLocator", vl.Substring(5, 7));
                            MessageBox.Show(vll);

                        };*/




                        //decoding the equipment
                        var eqp_factory = UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory as GalileoDesktopFactory;
                        string eqcommand = ".ED" + leg.Equipment.ToString() + "";
                        ReadOnlyCollection<string> eqp_col = eqp_factory.SendTerminalCommand(eqcommand);
                        var eqcount = 0;
                        foreach (var equip in eqp_col)
                        {
                            if (eqcount == 0)
                            {
                                legputobj.Add("equipment", equip);
                            }
                            eqcount++;
                        }

                        // leg
                        // Destination, DestinationTerminal,Equipment, FlightDuration, Origin, Origin Terminal can be found here
                        legArray.Add(legputobj);
                    }
                    jsegPt.Add("legs", legArray);


                    ReadOnlyCollection<BookingFileVendorLocator> VendorLocators = bf.VendorLocators;
                    foreach (BookingFileVendorLocator code in VendorLocators.Take(count))
                    {
                        var aircode = code.RecordLocator;
                        jsegPt.Add("VendorLocator", aircode);
                        MessageBox.Show(aircode);
                    }

                    


                    //add the ttb
                    var segc = airseg.SegmentNumber.ToString();
                    string command = "TTB" + segc + "";
                    var resCount = 0;
                    var seg_factory = UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory as GalileoDesktopFactory;
                    ReadOnlyCollection<string> seg_ttbt = seg_factory.SendTerminalCommand(command);
                    foreach (string s in seg_ttbt)
                    {
                        // MessageBox.Show("line number " + resCount + " " + s);
                        //add flightheader
                        if (resCount == 0)
                        {
                            jsegPt.Add("fheader", s.ToString());
                        }
                        //add duration
                        /*
                        if (resCount == 3)
                        {  
                            jsegPt.Add("data", s.ToString());
                        }
                         * */
                        resCount++;
                    }

                    jsegs.Add(jsegPt);
                }
                jobj.Add("segments", jsegs);






                var lobjFACT = UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory;
                var objPNR = lobjFACT.RetrieveCurrentBookingFile();
                if (objPNR.FiledFares.Count > 0)
                {
                    IEnumerator<AirFiledFareQuote> lobjAFFQ = objPNR.FiledFares.GetEnumerator();
                    Collection<int> filedFaresNum = new Collection<int>();
                    while (lobjAFFQ.MoveNext())
                    {
                        filedFaresNum.Add(lobjAFFQ.Current.Number);
                    }
                    objPNR = lobjFACT.RetrieveCurrentBookingFile(filedFaresNum);
                    if (objPNR != null && objPNR.FiledFares != null && objPNR.FiledFares.Count() > 0)
                    {
                        var fareArray = new JSONArray();
                        foreach (AirFiledFareQuote airfare in objPNR.FiledFares)
                        {
                            var putFare = new JSONObject();
                            var farenumber = airfare.Number.ToString();
                            putFare.Add("FQNum", "FQ" + airfare.Number.ToString());
                            putFare.Add("PlatingCarrier", (airfare.PlatingCarrier == null) ? " " : airfare.PlatingCarrier.ToString());
                            putFare.Add("StoredQuote", (airfare.StoredQuote == null) ? " " : airfare.StoredQuote.ToString());
                            putFare.Add("TicketingAgencyPcc", (airfare.TicketingAgencyPcc == null) ? " " : airfare.TicketingAgencyPcc.ToString());
                            putFare.Add("TicketingInformation", (airfare.TicketingInformation == null) ? " " : airfare.TicketingInformation.ToString());
                            MessageBox.Show(airfare.TicketingInformation);
                            putFare.Add("TicketingComments", (airfare.TicketingComments == null) ? " " : airfare.TicketingComments.ToString());
                            putFare.Add("ActualSellingFare", (airfare.ActualSellingFare == null) ? " " : airfare.ActualSellingFare.ToString());
                            putFare.Add("AccountingInfo", (airfare.AccountingInfo == null) ? " " : airfare.AccountingInfo.ToString());
                            putFare.Add("FareComments", (airfare.FareComments == null) ? " " : airfare.FareComments.ToString());
                            putFare.Add("EndorsementLine1", (airfare.EndorsementLine1 == null) ? " " : airfare.EndorsementLine1.ToString());
                            putFare.Add("EndorsementLine2", (airfare.EndorsementLine2 == null) ? " " : airfare.EndorsementLine2.ToString());
                            putFare.Add("EndorsementLine3", (airfare.EndorsementLine3 == null) ? " " : airfare.EndorsementLine3.ToString());

                            //get the filed fares in the farequote
                            var filedFare = new JSONArray();
                            ReadOnlyCollection<AirFiledFare> Fares = airfare.Fares;
                            foreach (AirFiledFare fares in Fares)
                            {
                                var putFiled = new JSONObject();
                                putFiled.Add("TAmt", fares.TotalAmount);
                                putFiled.Add("curr", fares.TotalAmountCurrency);
                                putFiled.Add("filDate", fares.FilingDate.ToString());
                                putFiled.Add("taxes", fares.TaxAmount);
                                putFiled.Add("StoredQuote", (fares.StoredQuote == null) ? " " : fares.StoredQuote.ToString());
                                putFiled.Add("FareConstruction", (fares.FareConstruction == null) ? " " : fares.FareConstruction.ToString());
                                //MessageBox.Show(fares.FareConstruction.ToString());

                                //MessageBox.Show("base fare in home country .. " + fares.EquivalentAmount);
                                string fareinhomecountry = fares.EquivalentAmount.ToString();
                                if (fareinhomecountry != null && fareinhomecountry != "")
                                {
                                    putFiled.Add("baseAmt", fareinhomecountry);
                                }

                                if (fareinhomecountry == "")
                                {
                                    putFiled.Add("baseAmt", fares.BaseAmount);
                                }



                                //get the taxes for the filed fare
                                var taxArray = new JSONArray();
                                ReadOnlyCollection<AirTax> Taxes = fares.Taxes;
                                foreach (AirTax tx in Taxes)
                                {
                                    var pt_tax = new JSONObject();
                                    pt_tax.Add("amount", tx.Amount);
                                    pt_tax.Add("code", tx.Code);
                                    pt_tax.Add("curr", fares.TotalAmountCurrency);
                                    pt_tax.Add("total", fares.TotalAmount);
                                    taxArray.Add(pt_tax);
                                }
                                //MessageBox.Show(taxArray[0].ToString() + taxArray[1].ToString() + taxArray[2].ToString() + taxArray[3].ToString());
                                putFiled.Add("taxes", taxArray);

                                //add the passangers affected by the filed fare
                                var paxfiled = new JSONArray();
                                ReadOnlyCollection<Person> paxFiledFare = fares.Passengers;
                                foreach (Person paxfare in paxFiledFare)
                                {
                                    var paxfObj = new JSONObject();
                                    paxfObj.Add("name", paxfare.FullName);
                                    paxfObj.Add("paxtype", paxfare.PassengerType.ToString());
                                    paxfiled.Add(paxfObj);
                                }

                                var bagAllow = new JSONArray();
                                ReadOnlyCollection<AirFiledFareSegment> faresegInfo = fares.SegmentInformation;
                                foreach (AirFiledFareSegment seginfo in faresegInfo)
                                {
                                    var ptbaggage = new JSONObject();
                                    string baggageallo = seginfo.BaggageAllowance.ToString();
                                    ptbaggage.Add("baggage", baggageallo);
                                    bagAllow.Add(ptbaggage);
                                }
                                putFiled.Add("baggage", bagAllow);


                                putFiled.Add("pax", paxfiled);
                                filedFare.Add(putFiled);
                            }

                            putFare.Add("fare", filedFare);

                            fareArray.Add(putFare);
                        }
                        jobj.Add("fares", fareArray);
                    }
                }


                else
                {
                }
                    
                }
            return jobj.ToString();
            }
        
        
        private void OnTerminalPreSendHandler(object sender, TerminalEventArgs e)
        {
           
            if (e.TerminalEntry.StartsWith("#TP", StringComparison.OrdinalIgnoreCase))
            {
                var window = UIHelper.Instance.CreateNewSmartBrowserWindow("BrowserWindow1");
                BrowserControl browser = new BrowserControl();
                var url = "http://192.168.0.58:8080/ticketproducer/processC";
                WebRequest req = WebRequest.Create(url);
                string postData = "bookingfile=" + getBookingFile() + "";
                byte[] send = System.Text.Encoding.Default.GetBytes(postData);
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = send.Length;

                Stream sout = req.GetRequestStream();
                sout.Write(send, 0, send.Length);
                browser.NavigateTo(url);
                window.Width = 1200;
                window.Height = 700;
                window.Content = browser;
                window.Title = "E-Ticket Producer";
                window.Show();
                sout.Flush();
                sout.Close();
            }

            if (e.TerminalEntry.StartsWith("#EDP", StringComparison.OrdinalIgnoreCase))
            {
                var window = UIHelper.Instance.CreateNewSmartBrowserWindow("BrowserWindow1");
                BrowserControl browser = new BrowserControl();
                var url = "http://192.168.0.9:8080/ticketproducer/processC";
                WebRequest req = WebRequest.Create(url);
                string postData = "bookingfile=" + getBookingFile() + "";
                byte[] send = System.Text.Encoding.Default.GetBytes(postData);
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = send.Length;

                Stream sout = req.GetRequestStream();
                sout.Write(send, 0, send.Length);
                browser.NavigateTo(url);
                window.Width = 1200;
                window.Height = 700;
                window.Content = browser;
                window.Title = "E-Ticket Producer";
                window.Show();
                sout.Flush();
                sout.Close();
            }

            if (e.TerminalEntry.StartsWith("#TV", StringComparison.OrdinalIgnoreCase))
            {
                var window = UIHelper.Instance.CreateNewSmartBrowserWindow("BrowserWindow1");
                BrowserControl browser = new BrowserControl();
                var url = "https://apps.travelport.co.ke/travelportapps/ticketproducer/processC";
                WebRequest req = WebRequest.Create(url);
                string postData = "bookingfile=" + getBookingFile() + "";
                byte[] send = System.Text.Encoding.Default.GetBytes(postData);
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = send.Length;

                Stream sout = req.GetRequestStream();
                sout.Write(send, 0, send.Length);
                browser.NavigateTo(url);
                window.Width = 1200;
                window.Height = 700;
                window.Content = browser;
                window.Title = "E-Ticket Producer";
                window.Show();
                sout.Flush();
                sout.Close();
            }
        }
         
        #endregion
    }
}
