﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Booking;
using Travelport.Smartpoint.Helpers.UI;
using Travelport.Smartpoint;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Collections.Specialized;

namespace Booking.Controls
{
    /// <summary>
    /// Interaction logic for UIControl.xaml
    /// </summary>
    public partial class UIControl : System.Windows.Controls.UserControl
    {
        Plugin plug = new Plugin();
        public UIControl()
        {
            InitializeComponent();
        }
        
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var window = UIHelper.Instance.CreateNewSmartBrowserWindow("BrowserWindow1");
         
            BrowserControl browser = new BrowserControl();
            var url = "http://192.168.0.152:8080/ticketproducer/processC"; 
            WebRequest req = WebRequest.Create(url);
            string postData = "bookingfile=" + plug.getBookingFile() + "";
            byte[] send = System.Text.Encoding.Default.GetBytes(postData);
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = send.Length;

            Stream sout = req.GetRequestStream();
            sout.Write(send, 0, send.Length);
            browser.NavigateTo(url);
            window.Width = 1200;
            window.Height = 700;
            window.Content = browser;
            window.Title = "E-Ticket Producer";
            window.Show();
            sout.Flush();
            sout.Close();
        }
    }
}
