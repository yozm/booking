﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FlowDocumentView.xaml.cs" company="Name of the company">
//    Copyright (c).  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for FlowDocumentView.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Booking.Views
{
    /// <summary>
    /// Interaction logic for FlowDocumentView.xaml
    /// </summary>
    public partial class FlowDocumentView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FlowDocumentView"/> class.
        /// </summary>
        public FlowDocumentView()
        {
            InitializeComponent();
        }
    }
}
