﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FlowDocumentItemView.xaml.cs" company="Name of the company">
//   Copyright (c).  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for FlowDocumentItemView.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Booking.Views
{
    /// <summary>
    /// Interaction logic for FlowDocumentItemView.xaml
    /// </summary>
    public partial class FlowDocumentItemView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FlowDocumentItemView"/> class.
        /// </summary>
        public FlowDocumentItemView()
        {
            InitializeComponent();
        }
    }
}
