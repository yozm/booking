﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RetrievePNRView.xaml.cs" company="Name of the company">
//    Copyright (c).  All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for RetrievePNRView.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Booking.Views
{
    /// <summary>
    /// Interaction logic for RetrievePNRView.xaml
    /// </summary>
    public partial class RetrievePNRView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RetrievePNRView"/> class.
        /// </summary>
        public RetrievePNRView()
        {
            InitializeComponent();
        }
    }
}
